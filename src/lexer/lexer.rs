/* 
 * schemeparser - scheme config serialization configurator
 * Copyright (C) 2021 Aleksandr Morozov, RELKOM s.r.o
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


use std::str::Chars;
use std::fmt;
use std::iter::Peekable;
use std::path::Path;
use std::fs::File;
use std::io::Read;
use std::borrow::Cow;

use crate::runtime_err::{SchemeResult, SchemeError, LexerError};

#[derive(PartialEq, Clone, Debug)]
pub enum Node 
{
    Identifier(String, LexerInfo),
    Integer(i64, LexerInfo),
    UInteger(u64, LexerInfo),
    Boolean(bool, LexerInfo),
    String(String, LexerInfo),
    List(Vec<Node>, LexerInfo),
}

impl fmt::Display for Node 
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result 
    {
        match *self 
        {
            Self::Identifier(ref s, ref li) => write!(f, "({}), Identifier: {}", li, s),
            Self::Integer(ref i, ref li) => write!(f, "({}), Integer: {}", li, i),
            Self::UInteger(ref u, ref li) => write!(f, "({}), UInteger: {}", li, u),
            Self::Boolean(ref b, ref li) => write!(f, "({}), Boolean: {}", li, b),
            Self::String(ref s, ref li) => write!(f, "({}), String: {}", li, s),
            Self::List(ref v, ref li) => 
            {
                write!(f, "({}), List: \n", li)?;
                for i in v.iter()
                {
                    write!(f, "{}", i)?;
                }
                write!(f, "")
            }
        }
    }
}

pub enum NumberSign
{
    Signed(i64),
    UnSigned(u64)
}

/// Contains a percise location of error in the schema.
/// Attention! If it is required to compare locations, do
/// not use PartialEq as it always returns true which is used
/// for debug purposes. Use is_eq() to compare instances.
#[derive(Clone, Copy, Debug)]
pub struct LexerInfo
{
    line: u32,
    column: u32
}

impl LexerInfo
{
    pub fn notexistent() -> Self
    {
        return Self {line: 0, column: 0};
    }

    pub fn is_eq(&self, other: &Self) -> bool
    {
        return self.column == other.column && self.line == other.line;
    }
}

impl PartialEq for LexerInfo 
{
    fn eq(&self, _other: &Self) -> bool 
    {
        return true;
    }
}


impl fmt::Display for LexerInfo 
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result 
    {
        write!(f, "line: {}, column: {}", self.line, self.column)
    }
}

pub struct Lexer<'lex>
{
    filepath: &'lex Path,
    chars: Peekable<Chars<'lex>>,
    curchar: Option<char>,
    li: LexerInfo,
}

impl<'lex> Lexer<'lex>
{
    /// Internal version of the Lexer. It is not the version in Cargo config!
    /// When making any changes which will affect somehow the output,
    /// change the version by 1
    pub fn version() -> i64
    {
        return 1;
    }

    pub fn from_file<P: AsRef<Path>>(filename: P) -> SchemeResult<Vec<Node>>
    {
        let mut file = 
            match File::open(filename.as_ref())
            {
                Ok(r) => r,
                Err(e) => 
                    return Err(
                            SchemeError::LexerErr(
                                LexerError::LexFileOpenErr(
                                    filename.as_ref().to_string_lossy().into_owned(), 
                                    e))),
            };

        let mut filecontent = String::new();
        file.read_to_string(&mut filecontent)?;

        //println!("debug:\n{}", filecontent);

        return Lexer::internal_from_str(&filecontent, filename);
    }

    pub fn from_str(schm: &str, strname: &'lex str) -> SchemeResult<Vec<Node>>
    {
        return Lexer::internal_from_str(schm, strname);
    }

    fn internal_from_str<P: AsRef<Path>>(schm: &str, filepath: P) -> SchemeResult<Vec<Node>>
    {
        let mut c = schm.chars().peekable();
        let mut line: u32 = 1;
        let cur: Option<char>;

        // skip all \d\a
        loop
        {
            match c.next()
            {
                Some(r) if r == '\x0a' =>
                {
                    line += 1;
                },
                Some(r) =>
                {
                    cur = Some(r);
                    break;
                },
                None => 
                    return Err(
                            SchemeError::LexerErr(
                                LexerError::LexEmpty(filepath.as_ref().to_path_buf()))),
            }            
        }

        let li = LexerInfo {line: line, column: 1};

        let mut lex = Lexer 
            {
                filepath: filepath.as_ref(),
                chars: c, 
                curchar: cur,
                li: li, 
            };

        let pres = lex.parsing(0)?;

        return Ok(pres);
    }

    #[inline] 
    fn move_next(&mut self)
    {
        if self.curchar == Some('\x0a')
        {
            //skip \d\a
            self.li.line += 1;
            self.li.column = 1;
        }
        else
        {
            self.li.column += 1;
            
        }

        self.curchar = self.chars.next();
    }

    #[inline]
    fn get_cur_char(&self) -> Option<char>
    {
        return self.curchar;
    }

    #[inline]
    fn foresee_char(&mut self) -> Option<char>
    {
        return match self.chars.peek()
        {
            Some(c) => Some(*c),
            None => None
        };
    }

    fn parsing(&mut self, depth: u64) -> SchemeResult<Vec<Node>>
    {
        let mut rootnode: Vec<Node> = Vec::new();
        loop 
        {
            match self.parse(depth)? 
            {
                Some(node) => 
                {
                    rootnode.push(node);
                },
                None => 
                {
                    return Ok(rootnode);
                }
            }
        }
    }

    fn parse(&mut self, depth: u64) -> SchemeResult<Option<Node>>
    {
        //let mut rootnode: Vec<Node> = Vec::new();

        loop
        {
            match self.get_cur_char()
            {
                None => 
                {
                    if depth == 0
                    {
                        //break;
                        return Ok(None);
                    }
                    else
                    {
                        return Err(
                                SchemeError::LexerErr(
                                    LexerError::LexUnexpEof(
                                        self.filepath.to_path_buf(), self.li, depth)));
                    }
                },
                Some(cc) =>
                {
                    match cc
                    {
                        _ if cc.is_whitespace() == true =>
                        {
                            self.move_next();
                        },
                        ';' =>
                        {
                            self.move_next();
                            loop
                            {
                                match self.get_cur_char()
                                {
                                    Some(c) if c == '\n' =>
                                    {
                                        //print!("{}", c);
                                        self.move_next();
                                        break;
                                    },
                                    Some(cc) => {/*print!("{}", cc);*/self.move_next();},
                                    None => break,
                                }
                            }
                        },
                        '(' =>
                        {
                            let li = self.li.clone();

                            // move to the next, skip (
                            self.move_next();

                            // go to the next List level
                            let res = self.parsing(depth + 1)?;
                            
                            // return to the parser with the result
                            return Ok( Some(Node::List(res, li) ));
                        }
                        ')' =>
                        {
                            self.move_next();
                            if depth > 0
                            {
                                return Ok(None);
                            }
                            else
                            {
                                return Err(
                                        SchemeError::LexerErr(
                                            LexerError::LexUnexpClosePattern(
                                                self.filepath.to_path_buf(), self.li)));
                            }
                        },
                        '\'' =>
                        {
                            let li = self.li.clone();

                            self.move_next();
                            let res = self.parse(depth)?;
                            match res
                            {
                                Some(r) => 
                                    return Ok(
                                        Some( 
                                            Node::List(
                                                vec![Node::Identifier("quote".to_string(), li.clone()), r], li) )),
                                None => 
                                    return Err(
                                        SchemeError::LexerErr(
                                            LexerError::LexMissing(
                                                self.filepath.to_path_buf(), "quote", self.li, depth)))
                            }                    
                        },
                        '`' =>
                        {
                            let li = self.li.clone();

                            self.move_next();
                            let res = self.parse(depth)?;
                            match res
                            {
                                Some(r) => 
                                    return Ok(
                                        Some( 
                                            Node::List(
                                                vec![Node::Identifier("quasiquote".to_string(), li.clone()), r], li) )),
                                None => 
                                    return Err(
                                        SchemeError::LexerErr(
                                            LexerError::LexMissing(
                                                self.filepath.to_path_buf(), "quasiquote", self.li, depth))),
                            }
                        },
                        ',' =>
                        {
                            let li = self.li.clone();

                            self.move_next();
                            let res = self.parse(depth)?;
                            match res
                            {
                                Some(r) => 
                                    return Ok(
                                        Some( 
                                            Node::List(
                                                vec![Node::Identifier("unquote".to_string(), li.clone()), r], li) )),
                                None =>
                                    return Err(
                                        SchemeError::LexerErr(
                                            LexerError::LexMissing(
                                                self.filepath.to_path_buf(), "unquote", self.li, depth))),
                            }
                        },
                        '+' | '-' =>
                        {
                            match self.foresee_char()
                            {
                                Some('0'..='9') =>
                                {
                                    let li = self.li.clone();

                                    //skip - symbol
                                    self.move_next();

                                    let rval = self.parse_number()?;
                                    let val = match rval
                                    {
                                        NumberSign::UnSigned(val) =>
                                        {
                                            if cc == '-'
                                            {
                                                //error, unsigned
                                                return Err(
                                                    SchemeError::LexerErr(
                                                        LexerError::LexSignUnsign(
                                                            self.filepath.to_path_buf(), self.li)));
                                            }

                                            //rootnode.push(Node::UInteger(val))
                                            Node::UInteger(val, li)
                                        },
                                        NumberSign::Signed(val) =>
                                        {
                                            let p = if cc == '-' 
                                            { 
                                                -1 * val 
                                            } 
                                            else 
                                            { 
                                                val 
                                            };

                                            //rootnode.push(Node::Integer(p));
                                            Node::Integer(p, li)
                                        }
                                    };
                                    
                                    self.check_delimiter()?;

                                    return Ok(Some(val));
                                }
                                _ => 
                                {
                                    let li = self.li.clone();

                                    // not followed by a digit, must be an identifier
                                    self.move_next();
                                    self.check_delimiter()?;
                                    return Ok(Some(Node::Identifier(cc.to_string(), li)));
                                }
                            }
                        },
                        '#' =>
                        {
                            let li = self.li.clone();
                            self.move_next();

                            let val = match self.get_cur_char()
                            {
                                Some('t') => Node::Boolean(true, li),
                                Some('f') => Node::Boolean(false, li),
                                _ => return Err(
                                                SchemeError::LexerErr(
                                                    LexerError::LexUnexpBoolean(
                                                        self.filepath.to_path_buf(), 
                                                        self.get_cur_char().unwrap(), 
                                                        self.li)
                                                    )
                                                ),
                            };

                            self.move_next();

                            return Ok(Some(val));
                        },
                        '0'..='9' =>
                        {
                            let li = self.li.clone();

                            let rval = self.parse_number()?;

                            let val = match rval
                            {
                                NumberSign::UnSigned(val) =>
                                {
                                    Node::UInteger(val, li)
                                },
                                NumberSign::Signed(val) =>
                                {
                                    Node::Integer(val, li)
                                }
                            };

                            self.check_delimiter()?;

                            return Ok(Some(val));
                        },
                        '\"' => 
                        {
                            let li = self.li.clone();
                            let val = self.parse_string()?;
                            self.check_delimiter()?;

                            return Ok(Some(Node::String(val, li)));
                        },
                        '[' | ']' | '{' | '}' | '|' | '\\' =>
                        {
                            return Err(
                                SchemeError::LexerErr(
                                    LexerError::LexUnexpBoolean(
                                        self.filepath.to_path_buf(),
                                        cc, 
                                        self.li)
                                    )
                                );
                        },
                        _ => 
                        {
                            let li = self.li.clone();

                            //identifier
                            let mut s = String::new();
                            loop 
                            {
                                match self.get_cur_char() 
                                {
                                    Some(c) => 
                                    {
                                        match c 
                                        {
                                            _ if c.is_whitespace() == true => 
                                            {
                                                break;
                                            },
                                            '(' | ')' | '[' | ']' | '{' | '}' | '\"' | ',' | '\'' | '`' | ';' | '|' | '\\' => 
                                            {
                                                break;
                                            },
                                            _ => 
                                            {
                                                s.push(c);
                                                self.move_next();
                                            },
                                        }
                                    },
                                    None => break
                                }
                            }

                            self.check_delimiter()?;
                            return Ok(Some(Node::Identifier(s, li)));
                        }
                    }
                }
            }
        } // loop

    }

    fn parse_string(&mut self) -> SchemeResult<String> 
    {
        self.move_next();

        let mut s = String::new();
        //let mut s = String::with_capacity(255);

        loop 
        {
            match self.get_cur_char() 
            {
                Some(c) => 
                {
                    match c 
                    {
                        '\"' => 
                        {
                            self.move_next();
                            break;
                        },
                        '\\' =>
                        {
                            self.move_next();
                            match self.get_cur_char()
                            {
                                Some(cc) =>
                                {
                                    match cc
                                    {
                                        '\"' => 
                                        {
                                            s.push(cc);
                                            self.move_next();
                                        },
                                        '\\' =>
                                        {
                                            s.push(cc);
                                            self.move_next();
                                        },
                                        _ =>
                                        {
                                            s.push('\\');
                                            s.push(cc);
                                            self.move_next();
                                        }
                                    }
                                },
                                _ =>
                                {
                                    return Err(
                                        SchemeError::LexerErr(
                                            LexerError::LexUnexpEolStr(
                                                self.filepath.to_path_buf(), s, self.li)));
                                }
                            }
                        }
                        _ => 
                        {
                            s.push(c);
                            self.move_next();
                        }
                    }
                },
                None => 
                    return Err(
                        SchemeError::LexerErr(
                            LexerError::LexEndOfQuoteExp(
                                self.filepath.to_path_buf(), s, self.li))),
            }
        }

        //s.shrink_to_fit();

        return Ok(s);
    }

    fn parse_number(&mut self) -> SchemeResult<NumberSign>
    {
        let mut s = String::new();
        let mut unsigned: bool = false;

        loop 
        {
            match self.get_cur_char() 
            {
                Some(c) => 
                {
                    match c 
                    {
                        '0'..='9' => //...
                        {
                            s.push(c);
                            self.move_next();
                        },
                        'U' | 'u' =>
                        {
                            unsigned = true;
                            self.move_next();
                        },
                        _ => break
                    }
                },
                None => break
            }
        }
        
        if unsigned == true
        {
            let res: u64 = match s.parse() 
            {
                Ok(value) => value,
                Err(_) => 
                    return Err(
                        SchemeError::LexerErr(
                            LexerError::LexNotAnInteger(
                                self.filepath.to_path_buf(), s, self.li))),
            };

            return Ok(NumberSign::UnSigned(res));
        }
        else
        {
            let res: i64 = match s.parse() 
            {
                Ok(value) => value,
                Err(_) => 
                    return Err(
                        SchemeError::LexerErr(
                            LexerError::LexNotAnInteger(
                                self.filepath.to_path_buf(), s, self.li))),
            };

            return Ok(NumberSign::Signed(res));
        }
    }

    #[inline]
    fn check_delimiter(&mut self) -> SchemeResult<()> 
    {
        match self.get_cur_char() 
        {
            Some(c) => 
            {
                match c 
                {
                    _ if c.is_whitespace() == true => (),
                    ')' => (),
                    _ => 
                        return Err(
                            SchemeError::LexerErr(
                                LexerError::LexDelimExp(
                                    self.filepath.to_path_buf(), c, self.li))),
                }
            },
            None => ()
        };

        return Ok(());
    }
}

#[test]
fn test1()
{

    let res = Lexer::from_str(r#"(int 1)(uint 1u)(string "string")(symbol symb1)(list '("abc" "def"))(listint '(1 2 3 4))(unq ,("abc" "def"))(bool #t)"#,"test").unwrap();
    let ali = LexerInfo{column: 0, line: 0};
    let v = vec![
        Node::List(vec![Node::Identifier("int".to_string(), ali.clone()), Node::Integer(1, ali.clone())], ali.clone()),
        Node::List(vec![Node::Identifier("uint".to_string(), ali.clone()), Node::UInteger(1, ali.clone())], ali.clone()),
        Node::List(vec![Node::Identifier("string".to_string(), ali.clone()), Node::String("string".to_string(), ali.clone())], ali.clone()),
        Node::List(vec![Node::Identifier("symbol".to_string(), ali.clone()), Node::Identifier("symb1".to_string(), ali.clone())], ali.clone()),
        Node::List(vec![
                        Node::Identifier("list".to_string(), ali.clone()), 
                        Node::List(vec![Node::Identifier("quote".to_string(), ali.clone()), 
                            Node::List(vec![Node::String("abc".to_string(), ali.clone()), Node::String("def".to_string(), ali.clone())], ali.clone())
                        ], ali.clone())
                    ], ali.clone()),
        Node::List(vec![
                        Node::Identifier("listint".to_string(), ali.clone()), 
                        Node::List(vec![Node::Identifier("quote".to_string(), ali.clone()), 
                            Node::List(vec![Node::Integer(1, ali.clone()), Node::Integer(2, ali.clone()), Node::Integer(3, ali.clone()), Node::Integer(4, ali.clone())], ali.clone())
                        ], ali.clone())
                    ], ali.clone()),
        Node::List(vec![
                        Node::Identifier("unq".to_string(), ali.clone()), 
                        Node::List(vec![Node::Identifier("unquote".to_string(), ali.clone()), 
                            Node::List(vec![Node::String("abc".to_string(), ali.clone()), Node::String("def".to_string(), ali.clone())], ali.clone())
                        ], ali.clone())
                    ], ali.clone()),
        Node::List(vec![Node::Identifier("bool".to_string(), ali.clone()), Node::Boolean(true, ali.clone())], ali.clone()),
    ];

    //assert_eq!(res, v);
    
    for (a, b) in res.into_iter().zip(v)
    {
        assert_eq!(a,b);
    }
}


#[test]
fn test2()
{
    let res = Lexer::from_str(r#"(test 1 symboltest (property/add 1 2u) (sub #t))"#, "test").unwrap();
    let ali = LexerInfo{column: 0, line: 0};
    let v = vec![
        Node::List(vec![
                        Node::Identifier("test".to_string(), ali.clone()), 
                        Node::Integer(1, ali.clone()),
                        Node::Identifier("symboltest".to_string(), ali.clone()),
                        Node::List(vec![
                            Node::Identifier("property/add".to_string(), ali.clone()),
                            Node::Integer(1, ali.clone()),
                            Node::UInteger(2, ali.clone()),
                        ], ali.clone()),
                        Node::List(vec![
                            Node::Identifier("sub".to_string(), ali.clone()),
                            Node::Boolean(true, ali.clone()),
                        ], ali.clone()),
        ], ali.clone())
    ];

    for (a, b) in res.into_iter().zip(v)
    {
        assert_eq!(a,b);
    }
}
