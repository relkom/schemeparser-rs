/* 
 * schemeparser - scheme config serialization configurator
 * Copyright (C) 2021 Aleksandr Morozov, RELKOM s.r.o
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

use crate::runtime_err::SchemeResult;
use crate::scheme::init::*;
use crate::scheme::dynamic_scheme::{dyn_run}; 
use crate::lexer::lexer::Lexer;
use linked_hash_map::LinkedHashMap;
use serde::{Serialize, Deserialize};

use std::borrow::Cow;
use std::time::Instant;

#[derive(Serialize, Deserialize)]
struct TestStruct
{
    cmd: Vec<String>,
}

#[derive(Serialize, Deserialize)]
struct MainTestStructure
{
    cmdstruct: TestStruct,
}

#[test]
fn test_dyn_serializer1()
{

    let teststr = TestStruct{cmd: vec!["mv".to_string(), "cp".to_string()]};
    let mainteststr = MainTestStructure{cmdstruct: teststr};
    let testser = serde_json::to_string(&mainteststr).unwrap();

    let t1 = 
r#"
(serializator "test"
    (define "move" "mv" '("commands"))
    (define "copy" "cp" '("commands"))
    (define "remove" "rm" '("commands"))
    
    (procedure "commands"
        (arg vector "command" strsymbol 
            (field/as 0u "cmd" (dt f/list f/string))
        )
    )

    (rootprocedure "commandstruct"
        (proc '("commands")
            ;(flags repeat)
            (field/as 0u "cmdstruct" (dt f/struct))
        )
    )
)"#;

    let v =
r#"
(commands '(move copy))
"#;

    let start = Instant::now();
        let last = Lexer::from_str(t1, "test").unwrap();
    let duration = start.elapsed();
    println!("Lexer: {:?}", duration);

    let start = Instant::now();
        let mut s = SchemeInit::new();
        let res = s.run(&last, None);
    let duration = start.elapsed();
    println!("Scheme pars: {:?}", duration);

    let start = Instant::now();
    let list = match res
    {
        Ok(r) => 
        {
            for (_k, v) in r.iter()
            {
            v.borrow().validate().unwrap();

            //dyn_print(&dynres);
            } 

            r
        },
        Err(e) =>
        {
            println!("err ({:?}): {}", e, e);
            panic!("e");
        }
    };

    let duration = start.elapsed();
    println!("Validation pars: {:?}", duration);

    let req = "test".to_string();
    let pars = list.get(&req).unwrap();

    let test1ast = Lexer::from_str(v, "test").unwrap();

    let start = Instant::now();
        let dynres = dyn_run(pars.clone(), &test1ast, "path").unwrap();
    let duration = start.elapsed();
    println!("dyn_run: {:?}", duration);
   // println!("{}\n{}",dynres, testser);
    assert_eq!(dynres, testser);
    
    let deser: MainTestStructure = serde_json::from_str(&dynres).unwrap();
}

#[test]
fn test_dyn_serializer2()
{
    let t1 = 
    r#"
    (serializator "test"
        (define "move" "mv" '("commands"))
        (define "copy" "cp" '("commands"))
        (define "remove" "rm" '("privcommands"))
        
        (procedure "privcommands"
            (arg vector "command" strsymbol 
                (field/as 0u "cmd" (dt f/list f/string))
            )
        )

        (procedure "commands"
            (arg vector "command" strsymbol 
                (field/as 0u "cmd" (dt f/list f/string))
            )
        )
    
        (rootprocedure "commandstruct"
            (proc '("commands")
                (field/as 0u "cmdstruct" (dt f/struct))
            )

            (proc '("privcommands")
                (field/as 1u "privcmdstruct" (dt f/struct))
            )
        )
    )"#;
    
        let v =
    r#"
    (commands '(move remove)) ; must fail
    (privcommands '(move remove)) ; must fail too, but never will be reached
    "#;
    
        let start = Instant::now();
            let last = Lexer::from_str(t1, "test").unwrap();
        let duration = start.elapsed();
        println!("Lexer: {:?}", duration);
    
        let start = Instant::now();
            let mut s = SchemeInit::new();
            let res = s.run(&last, None);
        let duration = start.elapsed();
        println!("Scheme pars: {:?}", duration);
    
        let start = Instant::now();
        let list = match res
        {
            Ok(r) => 
            {
                for (_k, v) in r.iter()
                {
                v.borrow().validate().unwrap();
    
                //dyn_print(&dynres);
                } 
    
                r
            },
            Err(e) =>
            {
                println!("err ({:?}): {}", e, e);
                panic!("e");
            }
        };
    
        let duration = start.elapsed();
        println!("Validation pars: {:?}", duration);
    
        let req = "test".to_string();
        let pars = list.get(&req).unwrap();
    
        let test1ast = Lexer::from_str(v, "test").unwrap();
    
        let start = Instant::now();
            let dynres = dyn_run(pars.clone(), &test1ast, "test");
        let duration = start.elapsed();
        println!("dyn_run: {:?}", duration);

        assert_eq!(dynres.is_err(), true);

        //test error todo
        println!("error: {}", dynres.err().unwrap());

        return;
}


#[test]
fn test_dyn_serializer3()
{
    #[derive(Serialize, Deserialize)]
    struct Test3Sub
    {
        cmd: Vec<String>,
    }

    #[derive(Serialize, Deserialize)]
    struct Test3
    {
        cmdstruct: Vec<String>,
        privcmdstruct: Test3Sub,
    }

    let t3s = Test3Sub{cmd: vec!["rm".to_string()]};
    let t3 = Test3{cmdstruct: vec!["mv".to_string(), "cp".to_string()], privcmdstruct: t3s };

    let t1 = 
    r#"
    (serializator "test"
        (define "move" "mv" '("commands"))
        (define "copy" "cp" '("commands"))
        (define "remove" "rm" '("privcommands"))
        
        (procedure "privcommands"
            (arg vector "command" strsymbol 
                (field/as 0u "cmd" (dt f/list f/string))
            )
        )

        (procedure "commands"
            (arg vector "command" strsymbol 
                (field/as 0u "cmd" (dt f/list f/string))
            )
        )
    
        (rootprocedure "commandstruct"
            (proc '("commands")
                (field/as 0u "cmdstruct" (dt f/field "cmd"))
            )

            (proc '("privcommands")
                (field/as 1u "privcmdstruct" (dt f/struct))
            )
        )
    )"#;
    
        let v =
    r#"
    (commands '(move copy)) ; must fail
    (privcommands '(remove)) ; must fail too, but never will be reached
    "#;
    
        let start = Instant::now();
            let last = Lexer::from_str(t1, "test").unwrap();
        let duration = start.elapsed();
        println!("Lexer: {:?}", duration);
    
        let start = Instant::now();
            let mut s = SchemeInit::new();
            let res = s.run(&last, None);
        let duration = start.elapsed();
        println!("Scheme pars: {:?}", duration);
    
        let start = Instant::now();
        let list = match res
        {
            Ok(r) => 
            {
                for (_k, v) in r.iter()
                {
                v.borrow().validate().unwrap();
    
                //dyn_print(&dynres);
                } 
    
                r
            },
            Err(e) =>
            {
                println!("err ({:?}): {}", e, e);
                panic!("e");
            }
        };
    
        let duration = start.elapsed();
        println!("Validation pars: {:?}", duration);
    
        let req = "test".to_string();
        let pars = list.get(&req).unwrap();
    
        let test1ast = Lexer::from_str(v, "test").unwrap();
    
        let start = Instant::now();
            let dynres = dyn_run(pars.clone(), &test1ast, "path");
        let duration = start.elapsed();
        println!("dyn_run: {:?}", duration);

        assert_eq!(dynres.is_err(), false);

        let dr = dynres.unwrap();
        let serialsd = serde_json::from_str(&dr);

        assert_eq!(serialsd.is_err(), false);

        let serzd:Test3  = serialsd.unwrap();

        let sd = serde_json::to_string(&t3).unwrap();

        assert_eq!(sd == dr, true);

        return;
}

#[test]
pub fn test_dyn_serializer4()
{
    let t1 = 
    r#"
    (serializator "test"

        (procedure "none" p/empty
            ;; todo
        )

        (procedure "path"
            (arg string "pathname"
                (field/as 0u "pathname" (dt f/string))
            )
        )

        (procedure "sqlite"
            (proc '("path")
                (field/as 0u "pathname" (dt f/field "pathname"))
            )
        ) 

        (procedure "database"
            (proc '("none" "sqlite")
                (field/as 0u "databasetype" 
                    (dt f/enum
                        (enum/as "none" "None")
                        (enum/as "sqlite" "Sqlite")
                    )
                )
            )
        )
        
        (rootprocedure "commandstruct"
            (proc '("database")
                (field/as 0u "database" (dt f/field "databasetype"))
                ; database: Option<EnumDatabase>
            )
        )
    )"#;
    
        let v =
    r#"
    (database (none))
    "#;

    let start = Instant::now();
            let last = Lexer::from_str(t1, "serializer_test").unwrap();
        let duration = start.elapsed();
        println!("Lexer: {:?}", duration);
    
        let start = Instant::now();
            let mut s = SchemeInit::new();
            let res = s.run(&last, None);
        let duration = start.elapsed();
        println!("Scheme pars: {:?}", duration);
    
        let start = Instant::now();
        let list = match res
        {
            Ok(r) => 
            {
                for (_k, v) in r.iter()
                {
                v.borrow().validate().unwrap();
    
                //dyn_print(&dynres);
                } 
    
                r
            },
            Err(e) =>
            {
                println!("err ({:?}): {}", e, e);
                panic!("e");
            }
        };
    
        let duration = start.elapsed();
        println!("Validation pars: {:?}", duration);
    
        let req = "test".to_string();
        let pars = list.get(&req).unwrap();
    
        let test1ast = Lexer::from_str(v, "serializer_test").unwrap();
    
        let start = Instant::now();
            let dynres = dyn_run(pars.clone(), &test1ast, "test").unwrap();
        let duration = start.elapsed();
        println!("dyn_run: {:?}", duration);

        println!("{}", dynres);

        return;
}
