/* 
 * schemeparser - scheme config serialization configurator
 * Copyright (C) 2021 Aleksandr Morozov, RELKOM s.r.o
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

use crate::runtime_err::SchemeResult;
use crate::scheme::init::*;
use crate::scheme::dynamic_scheme::{dyn_run}; 
use crate::lexer::lexer::Lexer;
use linked_hash_map::LinkedHashMap;
use serde::{Serialize, Deserialize};

use std::time::Instant;

#[derive(Serialize, Deserialize)]
enum RuleEnumeratorP
{
    All(Vec<RuleEnumeratorP>),
    Any(Vec<RuleEnumeratorP>),
    Not(Vec<RuleEnumeratorP>),
    Regex(String, Option<u64>, Option<String>),
    Literal(String, Option<u64>, Option<String>),
}

#[derive(Serialize, Deserialize)]
struct ActionStructP
{
    actiontitle: String,
    overrides: Option<LinkedHashMap<String, String>>, //Temp01Override.overkey:Temp01Override.overval
}

#[derive(Serialize, Deserialize)]
struct RulesetsStructP
{
    rulesetname: String,
    actions: Option<LinkedHashMap<String, ActionStructP>>, //ActionStructP.actiontitle
    rules: Vec<RuleEnumeratorP>,
}

#[derive(Serialize, Deserialize)]
struct SerRuleStructs
{
    rulesets: LinkedHashMap<String, RulesetsStructP>,
}

 #[test]
 fn test_dyn_deserializer1()
 {
    let as1 = ActionStructP{actiontitle: "action1".to_string(), overrides: None};
    let mut ovs2 = LinkedHashMap::new();
    ovs2.insert("keytest".to_string(), "valueresult".to_string());
    let as2 = ActionStructP{actiontitle: "action2".to_string(), overrides: Some(ovs2)};

    let mut acts1: LinkedHashMap<String, ActionStructP> = LinkedHashMap::new();
    acts1.insert("action1".to_string(), as1);
    acts1.insert("action2".to_string(), as2);

    let er1 = vec![
            RuleEnumeratorP::Any(vec![
                    RuleEnumeratorP::Regex("regex101".to_string(), Some(10), Some("regex 1 descr".to_string())),
                    RuleEnumeratorP::Regex("regex202".to_string(), Some(20), None),
                    RuleEnumeratorP::Not(vec![
                        RuleEnumeratorP::Literal("aux".to_string(), Some(15), None),
                    ])
            ]),
    ];

    let rs1 = RulesetsStructP{rulesetname: "ruleset1".to_string(), actions: Some(acts1), rules: er1};

    let er2 = vec![
            RuleEnumeratorP::Not(vec![
                RuleEnumeratorP::Literal("pass".to_string(), None, None),
            ])
    ];

    let rs2 = RulesetsStructP{rulesetname: "ruleset2".to_string(), actions: None, rules: er2};

    let mut rulesets: LinkedHashMap<String, RulesetsStructP> = LinkedHashMap::new();
    rulesets.insert("ruleset1".to_string(), rs1);
    rulesets.insert("ruleset2".to_string(), rs2);

    let rst = SerRuleStructs{rulesets: rulesets};

    let des = serde_json::to_string(&rst).unwrap();

let v = r#"(ruleset "ruleset1"
        (action "action1")
        (action "action2"
        (override "keytest" "valueresult")
    )

    (any
        (regex "regex101"
            (severity 10u)
            (descr "regex 1 descr")
        )
        (regex "regex202"
            (severity 20u)
        )
        (not
            (literal "aux"
                (severity 15u)
            )
        )
    )
)

(ruleset "ruleset2"
    (not 
        (literal "pass")
    )
)
"#;

let t1 = r#"
(serializator "test"

    (procedure "override"
        (arg string "overkey"
            (field/as 0u "overkey" (dt f/string))
        )
        (arg string "overval"
            (field/as 1u "overval" (dt f/string))
        )
    )


    (procedure "ruleset"
        (arg string "rulesetname"
            (field/as 0u "rulesetname" (dt f/string))
        )
        (proc '("action") 
            (flags repeat optional)
            (field/as 1u "actions" (dt f/hashmap "actiontitle" f/struct))
        ) 
        (proc '("all" "any" "not")
            (flags repeat optional)
            (field/as 2u "rules" 
                (dt f/list f/enum 
                    (enum/as "all" "All")
                    (enum/as "any" "Any")
                    (enum/as "not" "Not")
                )
            )
        ) 
    )

    (procedure "action"
        (arg string "actionname"
            (field/as 0u "actiontitle" (dt f/string))
        )
        (proc '("override")
            (flags repeat optional)
            (field/as 1u "overrides" (dt f/hashmap "overkey" f/field "overval"))
        )
    )

; see RuleEnumeratorP
    (procedure "all"
        (proc '("any" "not" "all" "regex" "literal")
            (flags repeat optional)
            (field/as 0u anon 
                (dt f/list f/enum
                    (enum/as "all" "All")
                    (enum/as "any" "Any")
                    (enum/as "not" "Not")
                    (enum/as "regex" "Regex")
                    (enum/as "literal" "Literal")
                )
            )
        ) 
    )

    (procedure "any"
        (proc '("any" "not" "all" "regex" "literal") 
            (flags repeat optional)
            (field/as 0u anon 
                (dt f/list f/enum
                    (enum/as "all" "All")
                    (enum/as "any" "Any")
                    (enum/as "not" "Not")
                    (enum/as "regex" "Regex")
                    (enum/as "literal" "Literal")
                )
            )
        )
    )

    (procedure "not"
        (proc '("any" "not" "all" "regex" "literal") 
            (flags repeat optional)
            (field/as 0u anon 
                (dt f/list f/enum
                    (enum/as "all" "All")
                    (enum/as "any" "Any")
                    (enum/as "not" "Not")
                    (enum/as "regex" "Regex")
                    (enum/as "literal" "Literal")
                )
            )
        )
    )

; temp
    (procedure "severity"
        (arg uinteger "sevlvl"
            (field/as 0u "sevvalue" (dt f/uinteger))
        )
    )

; temp
    (procedure "descr"
        (arg string "description"
            (field/as 0u "descrvalue" (dt f/string))
        )
    )

; temp
    (procedure "regex"
        (arg string "data"
            (field/as 0u "regexstr" (dt f/string))
        )
        (proc '("severity")
            (flags optional)
            (field/as 1u "sev" (dt f/field "sevvalue"))
        )
        (proc '("descr")
            (flags optional)
            (field/as 2u "descr" (dt f/field "descrvalue"))
        )
    )

    (procedure "literal"
        (arg string "data"
            (field/as 0u "regexstr" (dt f/string))
        )
        (proc '("severity")
            (flags optional)
            (field/as 1u "sev" (dt f/field "sevvalue"))
        )
        (proc '("descr")
            (flags optional)
            (field/as 2u "descr" (dt f/field "descrvalue"))
        )
    )

    (rootprocedure "filterstruct"
        (proc '("ruleset")
            (flags repeat)
            (field/as 0u "rulesets" (dt f/hashmap "rulesetname" f/struct))
        )
    )
)

"#;

let start = Instant::now();
        let last = Lexer::from_str(t1, "test").unwrap();
    let duration = start.elapsed();
    println!("Lexer: {:?}", duration);

    let start = Instant::now();
        let mut s = SchemeInit::new();
        let res = s.run(&last, None);
    let duration = start.elapsed();
    println!("Scheme pars: {:?}", duration);

    let start = Instant::now();
    let list = match res
    {
        Ok(r) => 
        {
            for (_k, v) in r.iter()
            {
            v.borrow().validate().unwrap();

            //dyn_print(&dynres);
            } 

            r
        },
        Err(e) =>
        {
            println!("err ({:?}): {}", e, e);
            panic!("e");
        }
    };
    let duration = start.elapsed();
    println!("Validation pars: {:?}", duration);

    let req = "test".to_string();
    let pars = list.get(&req).unwrap();

    let test1ast = Lexer::from_str(v, "test").unwrap();

    let start = Instant::now();
        let dynres = dyn_run(pars.clone(), &test1ast, "test").unwrap();
    let duration = start.elapsed();
    println!("dyn_run: {:?}", duration);

    assert_eq!(dynres, des);
    
    let deser: SerRuleStructs = serde_json::from_str(&dynres).unwrap();

    return;
}
