
/* 
 * schemeparser - scheme config serialization configurator
 * Copyright (C) 2021 Aleksandr Morozov, RELKOM s.r.o
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

use std::fmt;

use std::collections::HashSet;
use std::rc::Rc;
use std::cell::RefCell;
//use std::borrow::Cow;
use std::path::Path;

use linked_hash_map::LinkedHashMap;


use crate::lexer::lexer::{LexerInfo, Node};
use crate::runtime_err::{SchemeResult, SchemeError, SchemeRuntimeError};
use crate::scheme_runtime_error;

use super::serialization::*;
use super::init::Value;
use super::schemesubs::{SerializeAs, ProcVarType, SchemeProcedure, ProcVarTypeOpts, ProcVar, FieldVarType, FieldNameType, FieldVarArg};
use super::dynamic_common;
use super::dynamic_conversion;

#[derive(PartialEq, Clone)]
pub enum DynValue 
{
    Symbol(String, Option<LexerInfo>),
    Integer(i64, Option<LexerInfo>),
    UInteger(u64, Option<LexerInfo>),
    Boolean(bool, Option<LexerInfo>),
    String(String, Option<LexerInfo>),
    List(Vec<DynValue>, Option<LexerInfo>), 
    Procedure(DynFunction),
    ProcedureResult(ProcElements),
}

/// Converts the Value to the DynValue
impl From<Box<Value>> for DynValue 
{
    fn from(v: Box<Value>) -> DynValue
    {
        return DynValue::value2dynvalue(*v);
    }
}

impl DynValue
{
    /// Extracts LexerInfo structure by copying the original. If there is no
    /// LexerInfo available a default instance of LexerInfo is returned.
    pub fn extract_lexinfo(&self) -> LexerInfo
    {
        return match *self
        {
            Self::Symbol(_, ref li) => li.clone().unwrap_or(LexerInfo::notexistent()),
            Self::Boolean(_, ref li) => li.clone().unwrap_or(LexerInfo::notexistent()),
            Self::Integer(_, ref li) => li.clone().unwrap_or(LexerInfo::notexistent()),
            Self::UInteger(_, ref li) => li.clone().unwrap_or(LexerInfo::notexistent()),
            Self::List(_, ref li) => li.clone().unwrap_or(LexerInfo::notexistent()),
            Self::String(_, ref li) => li.clone().unwrap_or(LexerInfo::notexistent()),
            Self::Procedure(_) => LexerInfo::notexistent(),
            Self::ProcedureResult(_) => LexerInfo::notexistent(),
        };
    }

    pub fn extract_vec(self) -> SchemeResult<(Vec<DynValue>, Option<LexerInfo>)>
    {
        match self
        {
            DynValue::List(l, li) => return Ok((l, li)),
            _ => scheme_runtime_error!("Can not extrace vector from {}, near: {}", self, self.extract_lexinfo()),
        }
    }

    pub fn value2dynvalue(content: Value) -> Self
    {
        return match content
        {
            Value::Boolean(b, li) => DynValue::Boolean(b, Some(li)),
            Value::Integer(i, li) => DynValue::Integer(i, Some(li)),
            Value::UInteger(u, li) => DynValue::UInteger(u, Some(li)),
            Value::String(s, li) => DynValue::String(s, Some(li)),
            _ => panic!("Misuse! value2dynvalue can't convert {}", content),
        };
    }

    pub fn is_uinteger(&self) -> bool
    {
        if let DynValue::UInteger(_, _) = self
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    pub fn is_integer(&self) -> bool
    {
        if let DynValue::Integer(_, _) = self
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    pub fn is_string(&self) -> bool
    {
        if let DynValue::String(_, _) = self
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    pub fn is_list(&self) -> bool
    {
        if let DynValue::List(_, _) = self
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    pub fn get_list_itms(&self) -> std::slice::Iter<'_, DynValue>
    {
        if let DynValue::List(l, _) = self
        {
            return l.iter();
        }
        else
        {
            panic!("get_list_itm misuse, must be used only when DynValue::List!");
        }
    }

    pub fn is_list_q(&self) -> SchemeResult<bool>
    {
        if let DynValue::List(l, li) = self
        {
            if l.len() == 0
            {
                scheme_runtime_error!("Vector is empty, value: {}, near: {:?}", self, li);
            }

            let first = l.get(0).unwrap();
            match first
            {
                DynValue::Symbol(s, _) =>
                {
                    if s == "quoted" || s == "unquote"
                    {
                        return Ok(true);
                    }
                    else
                    {
                        return Ok(false);
                    }
                },
                _ => return Ok(false),
            }
        }
        else
        {
            return Ok(false);
        }
    }

    pub fn is_bool(&self) -> bool
    {
        if let DynValue::Boolean(_, _) = self
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    pub fn get_symbol(&self) -> &String
    {
        if let DynValue::Symbol(s, _) = self
        {
            s
        }
        else
        {
            panic!("Misuse get_symbol, requires  DynValue::Symbol");
        }
    }

    pub fn is_symbol(&self) -> bool
    {
        if let DynValue::Symbol(_, _) = self
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    pub fn from_nodes(nodes: &[Node]) -> Vec<DynValue> 
    {
        nodes.iter().map(DynValue::from_node).collect()
    }

    fn from_node(node: &Node) -> DynValue 
    {
        match *node 
        {
            Node::Identifier(ref val, ref li) => DynValue::Symbol(val.clone(), Some(li.clone())),
            Node::Integer(val, ref li) => DynValue::Integer(val, Some(li.clone())),
            Node::UInteger(val, ref li) => DynValue::UInteger(val, Some(li.clone())),
            Node::Boolean(val, ref li) => DynValue::Boolean(val, Some(li.clone())),
            Node::String(ref val, ref li) => DynValue::String(val.clone(), Some(li.clone())),
            Node::List(ref nodes, ref li) => DynValue::List(DynValue::from_nodes(&nodes), Some(li.clone()))
        }
    }
}

// type signature for all native functions

pub type ValueOperation = fn(&[DynValue], Rc<RefCell<DynEnvironment>>, oli: &Option<LexerInfo>) -> SchemeResult<DynValue>;

impl fmt::Display for DynValue 
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result 
    {
        match *self 
        {
            DynValue::Symbol(ref val, _) => write!(f, "Symbol '{}'", val),
            DynValue::Integer(val, _)    => write!(f, "Integer '{}'", val),
            DynValue::UInteger(val, _)    => write!(f, "UInteger '{}'", val),
            DynValue::Boolean(val, _)    => write!(f, "Boolean '{}'", if val { "t" } else { "f" }),
            DynValue::String(ref val, _) => write!(f, "String '{}'", val),
            DynValue::List(ref list, _)  => 
            {
                let strs: Vec<String> = list.iter().map(|v| format!("{}", v)).collect();
                write!(f, "List '({})'", &strs.join(" "))
            },
            DynValue::ProcedureResult(ref pe) =>
            {
                write!(f, "{}", pe)
            },
            _ =>  write!(f, "-other-"),
        }
    }
}

impl fmt::Debug for DynValue 
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result 
    {
        match *self 
        {
            DynValue::String(ref val, _) => write!(f, "\"{}\"", val),
            DynValue::List(ref list, _)  => 
            {
                let strs: Vec<String> = list.iter().map(|v| format!("{:?}", v)).collect();
                write!(f, "({})", &strs.join(" "))
            },
            /*DynValue::FuncRes(ref hs) => write!(f, "{:#?}", hs),*/
            _                      => write!(f, "{}", self)
        }
    }
}

pub enum DynFunction 
{
    /// A built-in constant procedures
    Native(ValueOperation),

    /// A dynamically initialized from the scheme
    Dynamic(Rc<SchemeProcedure>),
}

impl PartialEq for DynFunction 
{
    fn eq(&self, other: &DynFunction) -> bool 
    {
        self == other
    }
}

impl Clone for DynFunction 
{
    fn clone(&self) -> DynFunction 
    {
        match *self 
        {
            DynFunction::Native(ref func) => DynFunction::Native(*func),
            DynFunction::Dynamic(ref procinfo) => DynFunction::Dynamic(procinfo.clone()),
        }
    }
}

#[derive(PartialEq, Clone)] 
pub struct DynEnvironment 
{
    /// Serializator title
    intrpr_title: Rc<String>,

    /// Root procedure, a root structure
    rootproc: Rc<SchemeProcedure>,

    /// All regestered procedures
    values: LinkedHashMap<String, DynValue>,

    /// Defined values and its bindings to the procedures
    defbindings: LinkedHashMap<String, HashSet<String>>,
}

impl DynEnvironment 
{
    pub fn new_root(
                    intrpr_title: Rc<String>, 
                    rootproc: Rc<SchemeProcedure>,
                    mut values: LinkedHashMap<String, DynValue>,
                    defbindings: LinkedHashMap<String, HashSet<String>>
                    ) -> Rc<RefCell<DynEnvironment>> 
    {
        //declare static procedures
        // '() list
        values.insert(String::from("quote"), DynValue::Procedure(DynFunction::Native(native_quoted)));

        let env = DynEnvironment 
                    { 
                        intrpr_title: intrpr_title,
                        rootproc: rootproc,
                        values: values, 
                        defbindings: defbindings,
                    };

        return Rc::new(RefCell::new(env));
    }

    pub fn get(&self, key: &String) -> Option<DynValue> 
    {
        match self.values.get(key) 
        {
            Some(val) => return Some(val.clone()),
            None => return None,
        }
    }

    fn validate_proc(&self, sp: Rc<SchemeProcedure>) -> SchemeResult<()>
    {
        for ssp in sp.iter_procs()
        {
            for ssp_t in ssp.iter_procs_titles()
            {
                match self.values.get(ssp_t)
                {
                    Some(val) =>
                    {
                        match val
                        {
                            DynValue::Procedure(_) => {},
                            _ => scheme_runtime_error!("Unexpected value: {}, near: {}", val, val.extract_lexinfo())
                        }
                    },
                    None => scheme_runtime_error!("Value: {} not found", ssp_t),
                }
            }
        }

        return Ok(());
    }

    /// Performs validation of the Scheme which will be used to read configuration
    pub fn validate(&self) -> SchemeResult<()>
    {

        //check if (define ... ... "procs") exists
        for (_def_name, def_procs) in self.defbindings.iter()
        {
            for p in def_procs.iter()
            {
                if let Some(dp) = self.values.get(p)
                {
                    match dp
                    {
                        DynValue::Procedure(_) => {},
                        _ => scheme_runtime_error!("Unexpected dynvalue: {}, near: {}", dp, dp.extract_lexinfo()),
                    }
                }
                else
                {
                    return scheme_runtime_error!("Value: {} not found!", p);
                }
            }
            
        }

        self.validate_proc(self.rootproc.clone())?;

        for (_, v) in self.values.iter()
        {
            match v
            {
                DynValue::Procedure(func) =>
                {
                    match func 
                    {
                        DynFunction::Native(_) => {},
                        DynFunction::Dynamic(ref scheme_template) => 
                        {
                            self.validate_proc(scheme_template.clone())?;
                        }
                    }
                },
                _ => {}
            }
        }

        return Ok(());
    }

    /// Tests if the provided arguments matching with the scheme description
    pub fn check_var_type(&self, st: Rc<SchemeProcedure>, pv: &ProcVar, arg: &DynValue) -> SchemeResult<()>
    {
        let vt = pv.get_vartype(); 

        match vt
        {
            ProcVarType::Boolean =>
            {
                if arg.is_bool() == true
                {
                    return Ok(());
                }
            },
            ProcVarType::Integer =>
            {
                if arg.is_integer() == true
                {
                    return Ok(());
                }
            },
            ProcVarType::Uinteger =>
            {
                if arg.is_uinteger() == true
                {
                    return Ok(());
                }
            },
            ProcVarType::String => 
            {
                if arg.is_string() == true
                {
                    return Ok(());
                }
            },
            ProcVarType::IntSymbol  =>
            {
                if arg.is_symbol() == true
                {
                    let symb = arg.get_symbol();

                     //chack if it is allowed to use symbol in the procedure
                     if self.check_symbol_proc(st.get_title(), symb)? == false
                     {
                        scheme_runtime_error!("Symbol {} is not allowed in proc {} near {}", symb, st.get_title(), arg.extract_lexinfo());
                     }

                     return Ok(());
                }
                else if (arg.is_uinteger() == true) || (arg.is_integer() == true)
                {
                    return Ok(());
                }
            },
            ProcVarType::StrSymbol =>
            {
                if arg.is_symbol() == true
                {
                    let symb = arg.get_symbol();

                     //chack if it is allowed to use symbol in the procedure
                     if self.check_symbol_proc(st.get_title(), symb)? == false
                     {
                        scheme_runtime_error!("Symbol {} is not allowed in proc {} near {}", symb, st.get_title(), arg.extract_lexinfo());
                     }

                     return Ok(());
                }
                else if arg.is_string() == true
                {
                    return Ok(());
                }
            },
            ProcVarType::Vector =>
            {
                if arg.is_list() == false
                {
                    scheme_runtime_error!("Wrong datatype, expected 'vector' - '(), has {} near: {}", 
                                            arg, arg.extract_lexinfo());
                }

                let vto = pv.get_vartype_opts();

                if vto.is_none() == true
                {
                    scheme_runtime_error!("Misuse! check_var_type() \
                                            ProcVarType::Vector requires ProcVarTypeOpts, but \
                                            have {:?} {:?}, near: {}", vt, vto, arg.extract_lexinfo());
                }

                //get vector item
                for listval in arg.get_list_itms()
                {
                    let npv = ProcVar::new(
                                            vto.get_vector_pvt().clone(), 
                                            ProcVarTypeOpts::None, 
                                            SerializeAs::new_dummy());

                    self.check_var_type(st.clone(), &npv, listval)?;
                }

                return Ok(());
            
            },
            _ => scheme_runtime_error!("Misuse! check_var_type() \
                                        works only with specific VarTypes, but \
                                        have {:?}, near: {}", vt, arg.extract_lexinfo())
        }

        scheme_runtime_error!("Near {}, unexpected argument type: {} required {}", arg.extract_lexinfo(), arg, vt);
    }

    /// Veryfies if procedure can use symbol as argument in current Environment
    pub fn check_symbol_proc(&self, procname: &String, symbtitle: &String) -> SchemeResult<bool>
    {
        let procs = match self.defbindings.get(symbtitle)
        {
            Some(r) => r,
            None => 
                scheme_runtime_error!("symbol {} does not defined but used in procedure: '{}'", 
                                        symbtitle, procname),
        };

        if procs.is_empty() == true
        {
            // if list of procedures is empty then there is not constraints
            return Ok(true);
        }
        else
        {
            // check if procedure is in the list
            return Ok(procs.contains(procname));
        };
    }

    pub fn clone_root_struct(&self) -> Rc<SchemeProcedure>
    {
        return self.rootproc.clone();
    }
}

pub fn dyn_run<'p, P: AsRef<Path>>(root: Rc<RefCell<DynEnvironment>>, nodes: &[Node], filepath: P) -> SchemeResult<String>
{
    //convert nodes to DynValue
    let values = DynValue::from_nodes(nodes);

    // get the description of the root of the scheme
    let st = root.borrow().clone_root_struct();

    // initialize the vector of serialized and sorted data
    let order_serialized: Vec<Option<(String, SerDynValue)>> = vec![None; st.get_total_fields()];

    //read procedures in arbitrary order while not EOF
    let order_serialized = match read_procs(st.clone(), order_serialized, 0, &values, root.clone(), &None)
        {
            Ok(r) => r,
            Err(e) => scheme_runtime_error!("[dynscheme:{:?}]: {}", filepath.as_ref(), e),
        };

    let r = ProcElements::new(st.clone(), order_serialized)?;

    //return Ok(procargs);
    return Ok(serde_json::to_string(&r.eject_struct())?); 
}


pub fn evaluate_value(value: &DynValue, env: Rc<RefCell<DynEnvironment>>) -> SchemeResult<DynValue> 
{
    match value 
    {
        &DynValue::Symbol(ref v, ref li) => 
        {
            match env.borrow().get(v) 
            {
                Some(val) => Ok(val),
                None => 
                    scheme_runtime_error!("Identifier '{}' was not found for value: '{}' near: {:?}", 
                                            v, value, li),
            }
        },
        &DynValue::Integer(v, ref li) => Ok(DynValue::Integer(v, li.clone())),
        &DynValue::UInteger(v, ref li) => Ok(DynValue::UInteger(v, li.clone())),
        &DynValue::Boolean(v, ref li) => Ok(DynValue::Boolean(v, li.clone())),
        &DynValue::String(ref v, ref li) => Ok(DynValue::String(v.clone(), li.clone())),
        &DynValue::List(ref vec, ref li) => 
        {
            if vec.len() > 0 
            {
                evaluate_expression(vec, env.clone(), li)
            } 
            else 
            {
                //Ok(null!())
                scheme_runtime_error!("evaluation of value error, empty List at dynamic_scheme in scheme near {:?}", li);
            }
        },
        &DynValue::Procedure(ref v) => Ok(DynValue::Procedure(v.clone())),
       // &DynValue::ProcedureResult(ref t, ref a, ref p) => Ok(DynValue::ProcedureResult(t.clone(), .clone(), li.clone())),
        _ => scheme_runtime_error!("Can not evaluate Dynamically value: {}", value),
    }
}

fn evaluate_expression(values: &Vec<DynValue>, env: Rc<RefCell<DynEnvironment>>, oli: &Option<LexerInfo>) -> SchemeResult<DynValue> 
{
    if values.len() == 0 
    {
        scheme_runtime_error!("Empty expression, near: {:?}", oli);
    }

    let first = evaluate_value(&values[0], env.clone())?;
    match first 
    {
        DynValue::Procedure(func) =>
        {
            match func 
            {
                DynFunction::Native(native_fn) => 
                {
                    return native_fn(&values[1..], env, oli);
                },
                DynFunction::Dynamic(ref scheme_template) => 
                {
                    return run_dynamic_function(scheme_template.clone(), &values[1..], env.clone(), oli);
                }
            }
            //apply_function(&f, &values[1..], env.clone(), oli),
        },
        _ => scheme_runtime_error!("Incorrect preamble evaluated, must be 'procedure', got: '{}', near: {:?}", 
                                    first, oli),
    }
}

fn native_quoted(args: &[DynValue], _env: Rc<RefCell<DynEnvironment>>, oli: &Option<LexerInfo>) -> SchemeResult<DynValue> 
{
    if args.len() < 1 
    {
        scheme_runtime_error!("Too many arguments for native_quoted - '(), near {:?}", oli);
    }

    return Ok(args[0].clone());//DynValue::List(list, lli));
}

use super::dynamic_serialize::SerDynValue;

fn run_dynamic_function(st: Rc<SchemeProcedure>, args: &[DynValue], env: Rc<RefCell<DynEnvironment>>, oli: &Option<LexerInfo>) -> SchemeResult<DynValue>
{
    let mut argord: usize = 0;

    let mut order_serialized: Vec<Option<(String, SerDynValue)>> = vec![None; st.get_total_fields()];
    //Vec::with_capacity(st.get_total_fields());


    //read args first
    for (varname, varp) in st.get_args() 
    {
        if args.len() <= argord
        {
            //unexpected eof
            scheme_runtime_error!("Unexpected EOF while processing varname: '{}', in procedure: '{}', near: {:?}",
                                    varname, st.get_title(), oli);
        }

        //read arg
        let arg = &args[argord];

        let eval = evaluate_value(arg, env.clone())?;

        //check if datatype is satisfy the requested data
        env.borrow().check_var_type(st.clone(), varp, &eval)?;

        //try to store data as it is said in serializ
        let serlz = varp.get_seialz();

        match serlz.get_to_datatype()
        {   
            FieldVarArg::PrimArg(fvt) =>
            {
                let converted = dynamic_conversion::convert(fvt, eval)?;

                let m = order_serialized.get_mut(serlz.get_field_ord()).unwrap();
                *m = Some((serlz.clone_field_name(), converted));
            },
            FieldVarArg::List(fva) =>
            {
                let converted = dynamic_conversion::convert_list_items(fva, eval, env.clone())?;

                let m = order_serialized.get_mut(serlz.get_field_ord()).unwrap();
                *m = Some((serlz.clone_field_name(), converted));
            },
            _ => scheme_runtime_error!("(procedure \"{}\") can not serialize argument '{}' as {}, near: {}", 
                                        st.get_title(),
                                        varname, 
                                        serlz.get_to_datatype(), 
                                        arg.extract_lexinfo()),
        }

        argord += 1;
    }

    //read procedures in arbitrary order while not EOF
    order_serialized = read_procs(st.clone(), order_serialized, argord, args, env.clone(), oli)?;

    let r = ProcElements::new(st.clone(), order_serialized)?;

    return Ok(DynValue::ProcedureResult(r));
}

fn read_procs(st: Rc<SchemeProcedure>, 
                mut order_serialized: Vec<Option<(String, SerDynValue)>>,
                mut argord: usize, 
                args: &[DynValue], 
                env: Rc<RefCell<DynEnvironment>>, 
                oli: &Option<LexerInfo>) -> SchemeResult<Vec<Option<(String, SerDynValue)>>>
{
    //read procedures in arbitrary order while not EOF
    loop
    {
        if args.len() <= argord
        {
            break;
        }

        let arg = &args[argord];

        //evaluate current procedure
        let eval = evaluate_value(arg, env.clone())?;

        //unpack result
        let proc = match eval
        {
            DynValue::ProcedureResult(pe) => pe,
            _ => scheme_runtime_error!("eval: wrong datatype evaluated, expected DynValue::ProcedureResult, found {} near: {}",
                                        eval, arg.extract_lexinfo())
        };

        //get proc structure
        let proc_struct = match st.get_proc(proc.get_proc_name_str())
        {
            Some(p) => p,
            None => 
                scheme_runtime_error!("(process \"{}\") not found, expected procs: '{}', near: {}", 
                                        proc.get_proc_name_str(), 
                                        st.get_expected_procs(), 
                                        arg.extract_lexinfo())
        };

        //try to store data as it is said in serializ
        let serlz = proc_struct.get_proc_serialz();
        let flags = proc_struct.get_proc_flags();
        let osi = order_serialized.get_mut(serlz.get_field_ord()).unwrap();

        match serlz.get_to_datatype()
        {   
            FieldVarArg::Field(foregnfield_name) =>
            {
                if serlz.is_anonymous() == true
                {
                    scheme_runtime_error!("[f/field] can not be anonymous {}, near: {}", proc, arg.extract_lexinfo());
                }

                //get data from the field
                let (fkd, fkd_name) = 
                    dynamic_common::get_foregn_key_data(foregnfield_name, &proc)?;

                //try to store
                dynamic_common::store_field(osi, fkd, proc_struct)?;
            },
            FieldVarArg::Enum(alias) =>
            {
                //alias is enum/as
                //try to store
                dynamic_common::store_single_enum(alias, proc, osi, proc_struct)?;
            },
            FieldVarArg::Struct =>
            {
                if serlz.is_anonymous() == true
                {
                    scheme_runtime_error!("[f/struct] can not be anonymous {}, near: {}", proc, arg.extract_lexinfo());
                }

                let prepared_fkd = SerDynValue::Struct(proc.eject_struct());

                dynamic_common::store_field(osi, prepared_fkd, proc_struct)?;
            },
            FieldVarArg::List(boxed_fva) =>
            {
                match boxed_fva.as_ref()
                {
                    FieldVarArg::Field(foregnfield_name) =>
                    {
                        //get data from the field
                        let (foregndata, fkd_name) = 
                            dynamic_common::get_foregn_key_data(foregnfield_name, &proc)?;
                        
                        //store
                        dynamic_common::store_list(osi, foregndata, proc_struct)?;
                    },
                    FieldVarArg::Enum(alias) =>
                    {
                        //may be anonymous
                        dynamic_common::store_list_enum(&alias, proc, osi, proc_struct)?;
                    },
                    FieldVarArg::Struct =>
                    {
                        if serlz.is_anonymous() == true
                        {
                            scheme_runtime_error!("[f/struct] can not be anonymous {}, near: {}", proc, arg.extract_lexinfo());
                        }
                        
                        let foregndata = SerDynValue::Struct(proc.eject_struct());

                        //store
                        dynamic_common::store_list(osi, foregndata, proc_struct)?;
                    },
                    _ => scheme_runtime_error!("[f/list] unexpected datatype {}, near: {}", boxed_fva.as_ref(), arg.extract_lexinfo())
                }
            },
            FieldVarArg::HashMap(key, boxed_fva) =>
            {
                if serlz.is_anonymous() == true
                {
                    scheme_runtime_error!("[f/hashmap] can not be anonymous {}, near: {}", proc, arg.extract_lexinfo());
                }

                let fieldname = serlz.clone_field_name();

                //clone key from foregn structure
                let foregndata_key = match proc.clone_from_struct(key)
                {
                    Some(r) =>
                    {
                        if r.is_string() == false
                        {
                            scheme_runtime_error!("[f/hashmap] foreng key: {}, can only be type string, but have: {}, near {}", key, r, arg.extract_lexinfo());
                        }

                        r.take_string()
                    },
                    None => scheme_runtime_error!("[f/hashmap] foreng key: {}, does not exist in {}, near: {}", key, fieldname, arg.extract_lexinfo()),
                };

                //preparing right part
                match boxed_fva.as_ref()
                {
                    FieldVarArg::Field(foregnfield_name) =>
                    {
                        let (foregndata, fkd_name) = 
                            dynamic_common::get_foregn_key_data(foregnfield_name, &proc)?;

                        dynamic_common::store_hashmap(osi, foregndata, proc_struct, foregndata_key)?;
                    },
                    FieldVarArg::Enum(alias) =>
                    {
                        //may be anonymous
                        dynamic_common::store_hashmap_enum(&alias, proc, osi, proc_struct, foregndata_key)?;
                    },
                    FieldVarArg::Struct =>
                    {
                        let foregndata = SerDynValue::Struct(proc.eject_struct());

                        //try to store
                        dynamic_common::store_hashmap(osi, foregndata, proc_struct, foregndata_key)?;
                    },
                    FieldVarArg::List(boxed_fva) =>
                    {
                        match boxed_fva.as_ref()
                        {
                            FieldVarArg::Field(foregnfield_name) =>
                            {
                                //get data from the field
                                let (foregndata, fkd_name) = 
                                    dynamic_common::get_foregn_key_data(foregnfield_name, &proc)?;
                                
                                //store
                                dynamic_common::store_hashmap_list(osi, foregndata, proc_struct, foregndata_key)?;
                            },
                            FieldVarArg::Enum(alias) =>
                            {
                                //may be anonymous
                                dynamic_common::store_hashmap_list_enum(&alias, proc, osi, proc_struct, foregndata_key)?;
                            },
                            FieldVarArg::Struct =>
                            {
                                if serlz.is_anonymous() == true
                                {
                                    scheme_runtime_error!("can not be anonymous {}, near {}", proc, arg.extract_lexinfo());
                                }
                                
                                let foregndata = SerDynValue::Struct(proc.eject_struct());

                                //store
                                dynamic_common::store_hashmap_list(osi, foregndata, proc_struct, foregndata_key)?;
                            },
                            _ => scheme_runtime_error!("unexpected datatype: {}, near {}", boxed_fva.as_ref(), arg.extract_lexinfo())
                        }
                    },
                    _ => scheme_runtime_error!("unexpected datatype: {}, near {}", boxed_fva.as_ref(), arg.extract_lexinfo())
                }
            },
            _ => scheme_runtime_error!("is not possible in proc {}, near {}", serlz.get_to_datatype(), arg.extract_lexinfo()),
        }

        argord += 1;
    }

    return Ok(order_serialized);
}

