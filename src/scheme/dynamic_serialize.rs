/* 
 * schemeparser - scheme config serialization configurator
 * Copyright (C) 2021 Aleksandr Morozov, RELKOM s.r.o
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
use std::fmt;

use linked_hash_map::LinkedHashMap;
use serde::ser::{Serialize, Serializer, SerializeSeq, SerializeMap};

//use crate::runtime_err::{SchemeResult, SchemeError};

use super::dynamic_scheme::DynValue;

#[derive(Clone, PartialEq)]
pub enum SerDynValue
{
    Optional(Option<bool>),
    Integer(i64),
    UInteger(u64),
    Boolean(bool),
    String(String),
    List(Vec<SerDynValue>),
    Hashmap(LinkedHashMap<String, SerDynValue>),
    Struct(LinkedHashMap<String, SerDynValue>),
}

impl From<DynValue> for SerDynValue 
{
    fn from(v: DynValue) -> SerDynValue
    {
        match v
        {
            DynValue::Boolean(b, _) => return Self::Boolean(b),
            DynValue::Integer(i, _) => return Self::Integer(i),
            DynValue::UInteger(u, _) => return Self::UInteger(u),
            DynValue::String(s, _) => return Self::String(s),
            _ => panic!("From<DynValue> for SerDynValue misuse, can not convert {}", v),
        }
    }
}

impl From<Vec<DynValue>> for SerDynValue 
{
    fn from(v: Vec<DynValue>) -> SerDynValue
    {
        let c: Vec<SerDynValue> = v.into_iter().map(|x| x.into()).collect();
        return SerDynValue::List(c);
    }
}

impl fmt::Debug for SerDynValue
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result 
    {
        match *self
        {
            Self::Optional(_) => write!(f, "None"),
            Self::Integer(ref i) => write!(f, "{}", i),
            Self::UInteger(ref u) => write!(f, "{}", u),
            Self::Boolean(ref b) => write!(f, "{}", b),
            Self::String(ref s) => write!(f, "{}", s),
            Self::List(ref v) => write!(f, "{:?}", v),
            Self::Hashmap(ref lhm) => write!(f, "{:?}", lhm),
            Self::Struct(ref lhms) => write!(f, "{:?}", lhms),
        }
    }
}

impl fmt::Display for SerDynValue
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result 
    {
        match *self
        {
            Self::Optional(_) => write!(f, "Optional"),
            Self::Integer(_) => write!(f, "Integer"),
            Self::UInteger(_) => write!(f, "UInteger"),
            Self::Boolean(_) => write!(f, "Boolean"),
            Self::String(_) => write!(f, "String"),
            Self::List(_) => write!(f, "List"),
            Self::Hashmap(_) => write!(f, "Hashmap"),
            Self::Struct(_) => write!(f, "Struct"),
        }
    }
}

impl SerDynValue
{
    /*pub fn convert(dv: &DynValue) -> SchemeResult<Self>
    {
        match *dv
        {
            DynValue::Integer(i, _) => return Ok(SerDynValue::Integer(i)),
            DynValue::UInteger(u, _) => return Ok(SerDynValue::UInteger(u)),
            DynValue::Boolean(b, _) => return Ok(SerDynValue::Boolean(b)),
            DynValue::String(ref s, _) => return Ok(SerDynValue::String(s.clone())),
            DynValue::List(ref l, _) => 
            {
                let mut tmp: Vec<SerDynValue> = Vec::with_capacity(l.len());

                for val in l
                {
                    tmp.push(SerDynValue::convert(val)?);
                }

                return Ok(SerDynValue::List(tmp));
            },
           /* DynValue::FuncRes(ref f) => 
            {
                let mut tmp: LinkedHashMap<&'ser str, SerDynValue<'ser>> = LinkedHashMap::with_capacity(f.len());

                for (k, v) in f
                {
                    tmp.insert(k.as_str(), SerDynValue::convert(v)?);
                }
                return Ok(SerDynValue::FuncRes(tmp));
            },*/
            _ => return Err(SchemeError::DynUnexpDynVal(dv.clone(), None))
        }
    }*/

    pub fn unwrap_list(&mut self) -> &mut Vec<SerDynValue>
    {
        match *self
        {
            SerDynValue::List(ref mut l) => return l,
            _ => panic!("assertion: unwrap_list() misuse, wrong datatype: {}", self),
        }
    }

    pub fn unwrap_hashmap(&mut self) -> &mut LinkedHashMap<String, SerDynValue>
    {
        match *self
        {
            SerDynValue::Hashmap(ref mut l) => return l,
            _ => panic!("assertion: unwrap_hashmap() misuse, wrong datatype: {}", self),
        }
    }

    pub fn take_string(self) -> String
    {
        match self
        {
            SerDynValue::String(l) => return l,
            _ => panic!("assertion: take_string() misuse, wrong datatype: {}", self),
        }
    }

    pub fn is_string(&self) -> bool
    {
        match *self
        {
            SerDynValue::String(_) => return true,
            _ => return false
        }
    }
}


impl Serialize for SerDynValue
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        match *self 
        {
            SerDynValue::Optional(_) => serializer.serialize_none(),
            SerDynValue::Integer(i) => serializer.serialize_i64(i),
            SerDynValue::UInteger(u) => serializer.serialize_u64(u),
            SerDynValue::Boolean(b) => serializer.serialize_bool(b),
            SerDynValue::String(ref s) => serializer.serialize_str(s),
            SerDynValue::List(ref v) => 
            {
                let mut seq = serializer.serialize_seq(Some(v.len()))?;
                for element in v 
                {
                    seq.serialize_element(&element)?;
                }
                seq.end()
            },
            SerDynValue::Hashmap(ref h) =>
            {
                let mut map = serializer.serialize_map(Some(h.len()))?;
                for (k, v) in h 
                {
                    map.serialize_entry(k, &v)?;
                }
                map.end()
            },
            SerDynValue::Struct(ref h) =>
            {
                let mut map = serializer.serialize_map(Some(h.len()))?;
                for (k, v) in h 
                {
                    map.serialize_entry(k, &v)?;
                }
                map.end()
            }
        }
    }
}