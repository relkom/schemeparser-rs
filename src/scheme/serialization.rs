
/* 
 * schemeparser - scheme config serialization configurator
 * Copyright (C) 2021 Aleksandr Morozov, RELKOM s.r.o
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

use std::fmt;
use std::rc::Rc;

use linked_hash_map::LinkedHashMap;
use serde::Serialize;
//use serde::ser::{Serialize, Serializer, SerializeSeq, SerializeMap};

use crate::runtime_err::{SchemeResult, SchemeError, SchemeRuntimeError};
use crate::scheme_runtime_error;

use super::schemesubs::{SchemeProcedure, SerializeAs};
use super::dynamic_serialize::SerDynValue;
use super::dynamic_scheme::DynValue;

#[derive(PartialEq, Clone, Serialize)]
pub enum DataType
{
    Integer(i64),
    Uinteger(u64),
    String(String),
    Boolean(bool),
    Vector(Vec<DataType>),
}

impl fmt::Display for DataType 
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result 
    {
        match *self 
        {
            DataType::Integer(val)    => write!(f, "Integer '{}'", val),
            DataType::Uinteger(val)    => write!(f, "UInteger '{}'", val),
            DataType::Boolean(val)    => write!(f, "Boolean '{}'", if val { "t" } else { "f" }),
            DataType::String(ref val) => write!(f, "String '{}'", val),
            DataType::Vector(ref list)  => 
            {
                let strs: Vec<String> = list.iter().map(|v| format!("{}", v)).collect();
                write!(f, "Vector '({})'", &strs.join(" "))
            },
        }
    }
}

impl fmt::Debug for DataType 
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result 
    {
        match *self 
        {
            DataType::Integer(val)    => write!(f, "Integer '{}'", val),
            DataType::Uinteger(val)    => write!(f, "UInteger '{}'", val),
            DataType::Boolean(val)    => write!(f, "Boolean '{}'", if val { "t" } else { "f" }),
            DataType::String(ref val) => write!(f, "String '{}'", val),
            DataType::Vector(ref list)  => 
            {
                let strs: Vec<String> = list.iter().map(|v| format!("{}", v)).collect();
                write!(f, "Vector '({})'", &strs.join(" "))
            },
        }
    }
}

impl DataType
{
    pub fn dynval2datatype(dv: DynValue) -> Self
    {
        match dv
        {
            DynValue::Integer(i, _) => DataType::Integer(i),
            DynValue::UInteger(u, _) => DataType::Uinteger(u),
            DynValue::String(s, _) => DataType::String(s),
            DynValue::Boolean(b, _) => DataType::Boolean(b),
            DynValue::List(l, _) => 
            {
                let mut temp = Vec::<DataType>::with_capacity(l.len());
                for item in l
                {
                    temp.push(DataType::from(item));
                }

                DataType::Vector(temp)
            },
            _ => panic!("dynval2datatype: can not convert {}", dv),
        }
        
    }
}

impl From<DynValue> for DataType 
{
    fn from(v: DynValue) -> DataType
    {
        return DataType::dynval2datatype(v);
    }
}

#[derive(Clone, PartialEq)]
pub struct ProcElements
{
    st: Rc<SchemeProcedure>, 
    sorted_sturct: LinkedHashMap<String, SerDynValue>,
}

impl ProcElements
{
    pub fn new(st: Rc<SchemeProcedure>, 
                mut order_serialized: Vec<Option<(String, SerDynValue)>>) -> SchemeResult<Self>
    {
        //verify that all fields present and insert optional one
        for proc in st.iter_procs()
        {
            let flag = proc.get_proc_flags();
            let serlz = proc.get_proc_serialz();
            
            let os = order_serialized.get_mut(serlz.get_field_ord()).unwrap();

            if os.is_none() == true
            {
                //check if field is optional
                if flag.is_optional() == false
                {
                    //missing non optional field
                    scheme_runtime_error!("missing non-optional field '{}', in procedure: '{}'", proc, st.get_title());
                }
                else
                {
                    *os = Some( ( serlz.clone_field_name(), SerDynValue::Optional(None) ) );
                }
            }

            /*match os
            {
                Some(_) => {},
                None =>
                {
                    //check if field is optional
                    if flag.is_optional() == false
                    {
                        //missing non optional field
                        panic!("todo: missing field {}", proc);
                    }
                    else
                    {
                        //insert
                        
                        /*order_serialized.insert(
                                serlz.get_field_ord(),
                                Some( ( serlz.clone_field_name(), SerDynValue::Optional(None) ) )
                            );*/
                    }
                }
            }*/
        }

        let mut sorted_sturct: LinkedHashMap<String, SerDynValue> = LinkedHashMap::with_capacity(order_serialized.len());
        let mut index = 0;

        //convert to structure
        for os in order_serialized.drain(..)
        {
            match os
            {
                None => scheme_runtime_error!("mandatory field index: {} is not present in procedure: {}", index, st.get_title()),
                Some((title, sdv)) =>
                {
                    index += 1;
                    /*if &title != "~anonymous"
                    {*/
                        sorted_sturct.insert(title, sdv);
                    //}
                   /* else
                    {
                        match sdv
                        {
                            SerDynValue::List(m) =>
                            {

                                match m
                                {
                                    SerDynValue::Hashmap(hm) =>
                                    {
                                        sorted_sturct.extend(m);
                                    },
                                    _ => panic!("todo: must be hashmap!")
                                }
                                
                            },
                            _ => panic!("todo: ProcElements::new, hashmap only for anon"),
                        }
                        
                    }*/
                }
            }
        }
        
        return Ok(Self {st: st.clone(), sorted_sturct: sorted_sturct});
    }

    pub fn get_proc_name(&self) -> &str
    {
        return &self.st.get_title();
    }

    pub fn get_proc_name_str(&self) -> &String
    {
        return &self.st.get_title();
    }

    pub fn clone_proc_name(&self) -> String
    {
        return self.st.clone_title();
    }

    pub fn clone_from_struct(&self, key: &String) -> Option<SerDynValue>
    {
        match self.sorted_sturct.get(key)
        {
            Some(r) => return Some(r.clone()),
            None => return None,
        }
    }

    pub fn eject_struct(self) -> LinkedHashMap<String, SerDynValue>
    {
        return self.sorted_sturct;
    }
}

impl fmt::Display for ProcElements 
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result 
    {
        write!(f, "procedure title: {}\n struct:\n {:?}", self.get_proc_name(), self.sorted_sturct)
    }
}

impl fmt::Debug for ProcElements 
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result 
    {
        write!(f, "procedure title: {}\n struct:\n {:?}", self.get_proc_name(), self.sorted_sturct)
    }
}