
/* 
 * schemeparser - scheme config serialization configurator
 * Copyright (C) 2021 Aleksandr Morozov, RELKOM s.r.o
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

use std::fmt;

use std::collections::{/*HashMap,*/ HashSet};
use linked_hash_map::LinkedHashMap;
use std::rc::Rc;
use std::cell::RefCell;

//use rusty_scheme::reader::parser::*;

use crate::lexer::lexer::{LexerInfo, Node};
use crate::runtime_err::{SchemeResult, SchemeError, SchemeRuntimeError, convert_list, convert_hashset};
use crate::scheme_runtime_error;
use super::schemesubs::{ProcVarType, SchemeProc, SchemeProcedure, SchemeArgFlags, ProcVarTypeOpts, ProcVar, SerializeAs, FieldVarArg, FieldNameType, FieldVarType};
use super::dynamic_scheme::{DynValue, DynFunction, DynEnvironment};
use super::common::{CommonReader, Contains, find_dups_for_ord_num};


#[derive(Clone)]
pub enum Value 
{
    Symbol(String, LexerInfo),
    Integer(i64, LexerInfo),
    UInteger(u64, LexerInfo),
    Boolean(bool, LexerInfo),
    String(String, LexerInfo),
    List(Vec<Value>, LexerInfo),
    Procedure(Function),
    ProcVarEnum(ProcVarType),
    FieldVarEnum(FieldVarType),
    FieldNameEnum(FieldNameType),
    Macro(Vec<String>, Vec<Value>), //rem
    RetDefine(String, Box<Value>, Option<HashSet<String>>),
    RetArg(String, ProcVarType, ProcVarTypeOpts, SerializeAs),
    RetProc(HashSet<String>, SchemeArgFlags, SerializeAs),
    RetFlags(SchemeArgFlags),
    RetProcedure(SchemeProcedure),
    RetRootProcedure(SchemeProcedure),
    ResInterpritator(Rc<String>, Rc<RefCell<DynEnvironment>>),
    RetFieldAs(SerializeAs),
    RetEnumAs(String, String),
    ResVerion(i64),
    /// ..., to_datatype, key, val, enumas
    RetDt(FieldVarArg),
    RepeatRet,
    OptionalRet,
    RestrictRet,
    ProcEmpty,
}

impl Value
{
    pub fn is_proc_empty(&self) -> bool
    {
        match *self
        {
            Self::ProcEmpty => return true,
            _ => return false,
        }
    }

    pub fn extract_lexerinfo(&self) -> LexerInfo
    {
        match *self
        {
            Self::Symbol(_, ref li) => li.clone(),
            Self::Integer(_, ref li) => li.clone(),
            Self::UInteger(_, ref li) => li.clone(),
            Self::Boolean(_, ref li) => li.clone(),
            Self::String(_, ref li) => li.clone(),
            Self::List(_, ref li) => li.clone(),
            _ => LexerInfo::notexistent(),
        }
    }
}

// type signature for all native functions
pub type ValueOperation = fn(&[Value], Rc<RefCell<Environment>>, li: &LexerInfo) -> SchemeResult<Value>;

pub enum Function 
{
    Native(ValueOperation),
    Scheme(Vec<String>, Vec<Value>, Rc<RefCell<Environment>>),
}

impl Value 
{
    pub fn from_nodes(nodes: &[Node]) -> Vec<Value> 
    {
        nodes.iter().map(Value::from_node).collect()
    }

    fn from_node(node: &Node) -> Value 
    {
        match *node 
        {
            Node::Identifier(ref val, ref li) => Value::Symbol(val.clone(), li.clone()),
            Node::Integer(val, ref li) => Value::Integer(val, li.clone()),
            Node::UInteger(val, ref li) => Value::UInteger(val, li.clone()),
            Node::Boolean(val, ref li) => Value::Boolean(val, li.clone()),
            Node::String(ref val, ref li) => Value::String(val.clone(), li.clone()),
            Node::List(ref nodes, ref li) => Value::List(Value::from_nodes(&nodes), li.clone())
        }
    }
}

impl fmt::Display for Value 
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result 
    {
        match *self 
        {
            Value::Symbol(ref val, _) => write!(f, "{}", val),
            Value::Integer(val, _)    => write!(f, "{}", val),
            Value::UInteger(val, _)    => write!(f, "{}", val),
            Value::Boolean(val, _)    => write!(f, "#{}", if val { "t" } else { "f" }),
            Value::String(ref val, _) => write!(f, "{}", val),
            Value::List(ref list, ref li)  => 
            {
                let strs: Vec<String> = list.iter().map(|v| format!("{}", v)).collect();
                write!(f, "[{}]({})", li, &strs.join(" "))
            },
            Value::Procedure(_)   => write!(f, "#<procedure>"),
            Value::Macro(_,_)     => write!(f, "#<macro>"),
            Value::ProcVarEnum(ref val) => write!(f, "ProcVarEnum {}", val),
            _ =>  write!(f, "-other-"),
        }
    }
}

impl fmt::Debug for Value 
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result 
    {
        match *self 
        {
            Value::String(ref val, ref li) => write!(f, "[{}]\"{}\"", li, val),
            Value::List(ref list, ref li)  => 
            {
                let strs: Vec<String> = list.iter().map(|v| format!("{:?}", v)).collect();
                write!(f, "[{}]({})", li, &strs.join(" "))
            },
            _ => write!(f, "{}", self)
        }
    }
}


impl PartialEq for Function 
{
    fn eq(&self, other: &Function) -> bool 
    {
        self == other
    }
}

impl Clone for Function 
{
    fn clone(&self) -> Function 
    {
        match *self 
        {
            Function::Native(ref func) => Function::Native(*func),
            Function::Scheme(ref a, ref b, ref env) => Function::Scheme(a.clone(), b.clone(), env.clone())
        }
    }
}

pub struct Environment 
{
    values: LinkedHashMap<String, Value>,
}

impl Environment 
{
    pub fn new_root() -> Rc<RefCell<Environment>> 
    {
        let env = Environment { values: LinkedHashMap::new() };
        return Rc::new(RefCell::new(env));
    }

    pub fn define<S: Into<String>>(&mut self, key: S, value: Value) -> SchemeResult<()> 
    {
        let k: String = key.into();
        if self.values.contains_key(&k) 
        {
            scheme_runtime_error!("Duplicate environment define {}", k);
        } 
        else 
        {
            self.values.insert(k, value);
            Ok(())
        }
    }

    pub fn get(&self, key: &String) -> Option<Value> 
    {
        match self.values.get(key) 
        {
            Some(val) => return Some(val.clone()),
            None => return None
        }
    }

}

pub fn evaluate_value(value: &Value, env: Rc<RefCell<Environment>>) -> SchemeResult<Value> 
{
    match value 
    {
        &Value::Symbol(ref v, ref li) => 
        {
            match env.borrow().get(v) 
            {
                Some(val) => Ok(val),
                None => scheme_runtime_error!("Indentifier: '{}' not found, near: {}",
                        v, li),
            }
        },
        &Value::Integer(v, ref li) => Ok(Value::Integer(v, li.clone())),
        &Value::Boolean(v, ref li) => Ok(Value::Boolean(v, li.clone())),
        &Value::String(ref v, ref li) => Ok(Value::String(v.clone(), li.clone())),
        &Value::List(ref vec, ref li) => 
        {
            if vec.len() > 0 
            {
                evaluate_expression(vec, env.clone(), li)
            } 
            else 
            {
                //Ok(Value::List(vec![]))
                scheme_runtime_error!("Value::List is empty info: {}, near: {}", value, li);
            }
        },
        &Value::Procedure(ref v) => Ok(Value::Procedure(v.clone())),
        &Value::Macro(ref a, ref b) => Ok(Value::Macro(a.clone(), b.clone())),
        &Value::ProcVarEnum(ref v) => Ok(Value::ProcVarEnum(v.clone())),
       // &Value::CompEnum(ref v) => Ok(Value::CompEnum(v.clone())),
        _ => scheme_runtime_error!("Can not evaluate '{}', not implemented, near: {}", 
                                    value, value.extract_lexerinfo()),
    }
}

pub fn evaluate_expression(values: &Vec<Value>, env: Rc<RefCell<Environment>>, li: &LexerInfo) -> SchemeResult<Value> 
{
    if values.len() == 0 
    {
        scheme_runtime_error!("Can't evaluate an empty expression: {}", convert_list(values));
    }

    let first = evaluate_value(&values[0], env.clone())?;
    match first 
    {
        Value::Procedure(f) => apply_function(&f, &values[1..], env.clone(), li),
        Value::Macro(_, _) => panic!("macroses are not allowed"),
        _ => scheme_runtime_error!("Can not evaluate expression because value type is \
                                    unknown, got: '{}', near {}",
                                    first, first.extract_lexerinfo()),
    }
}

fn apply_function(func: &Function, args: &[Value], env: Rc<RefCell<Environment>>, li: &LexerInfo) -> SchemeResult<Value> 
{
    match func 
    {
        &Function::Native(native_fn) => 
        {
            return native_fn(args, env, li);
        },
        &Function::Scheme(ref _arg_names, ref _body, ref _func_env) => 
        {
            scheme_runtime_error!("Can not apply in-schema defined function \
                                    because not implemented, near {}", 
                                    li);
        }
    }
}


#[derive(Clone)]
pub struct SchemeInit 
{
    root: Rc<RefCell<Environment>>,
}

impl SchemeInit 
{
    const ATTR_REPEAT: &'static str = "repeat";
    //const ATTR_COMBINE: &'static str = "combine";
    const ATTR_OPTIONAL: &'static str = "optional";
    const ATTR_RESTRICT: &'static str = "restrict";

    const PROC_ATTR_RESTRICT: &'static str = "p/empty";

    pub fn new() -> Self 
    {
        let root = Environment::new_root();

        let mut env = root.borrow_mut();

        env.define("quote", Value::Procedure(Function::Native(native_quoted))).unwrap();
        env.define("version", Value::Procedure(Function::Native(f_version))).unwrap();

        env.define("serializator", Value::Procedure(Function::Native(f_serializator))).unwrap();
        env.define("rootprocedure", Value::Procedure(Function::Native(f_rootprocedure))).unwrap();
        env.define("procedure", Value::Procedure(Function::Native(f_procedure))).unwrap();
        env.define("arg", Value::Procedure(Function::Native(f_arg))).unwrap();
        env.define("proc", Value::Procedure(Function::Native(f_proc))).unwrap();
        env.define("define", Value::Procedure(Function::Native(f_define))).unwrap();
        env.define("flags", Value::Procedure(Function::Native(f_flags))).unwrap();

        env.define("field/as", Value::Procedure(Function::Native(f_field_as))).unwrap();
        env.define("enum/as", Value::Procedure(Function::Native(f_enum_as))).unwrap();
        env.define("dt", Value::Procedure(Function::Native(f_dt))).unwrap();

        env.define("anon", Value::FieldNameEnum(FieldNameType::Anon)).unwrap();

        env.define("string", Value::ProcVarEnum(ProcVarType::String)).unwrap();
        env.define("integer", Value::ProcVarEnum(ProcVarType::Integer)).unwrap();
        env.define("uinteger", Value::ProcVarEnum(ProcVarType::Uinteger)).unwrap();
        env.define("boolean", Value::ProcVarEnum(ProcVarType::Boolean)).unwrap();
        env.define("strsymbol", Value::ProcVarEnum(ProcVarType::StrSymbol)).unwrap();
        env.define("intsymbol", Value::ProcVarEnum(ProcVarType::IntSymbol)).unwrap();
        env.define("vector", Value::ProcVarEnum(ProcVarType::Vector)).unwrap();

        env.define("f/string", Value::FieldVarEnum(FieldVarType::String)).unwrap();
        env.define("f/integer", Value::FieldVarEnum(FieldVarType::Integer)).unwrap();
        env.define("f/uinteger", Value::FieldVarEnum(FieldVarType::Uinteger)).unwrap();
        env.define("f/boolean", Value::FieldVarEnum(FieldVarType::Boolean)).unwrap();
        env.define("f/list", Value::FieldVarEnum(FieldVarType::List)).unwrap();
        env.define("f/hashmap", Value::FieldVarEnum(FieldVarType::HashMap)).unwrap();
        env.define("f/struct", Value::FieldVarEnum(FieldVarType::Struct)).unwrap();
        env.define("f/enum", Value::FieldVarEnum(FieldVarType::Enum)).unwrap();
        env.define("f/field", Value::FieldVarEnum(FieldVarType::Field)).unwrap();

        env.define(SchemeInit::ATTR_REPEAT, Value::RepeatRet).unwrap();
        env.define(SchemeInit::ATTR_OPTIONAL, Value::OptionalRet).unwrap();
        env.define(SchemeInit::ATTR_RESTRICT, Value::RestrictRet).unwrap();

        env.define(SchemeInit::PROC_ATTR_RESTRICT, Value::ProcEmpty).unwrap();

        drop(env);

        return Self { root: root };
    }

    pub fn run(&mut self, nodes: &[Node], confver: Option<i64>) -> SchemeResult<LinkedHashMap<Rc<String>, Rc<RefCell<DynEnvironment>>>> 
    {
        let values = Value::from_nodes(nodes);

        match evaluate_values(&values, self.root.clone(), confver)
        {
            Ok(r) => return Ok(r),
            Err(e) => scheme_runtime_error!("[init] {}", e),
        }
    }

}

fn evaluate_values(values: &[Value], 
                    env: Rc<RefCell<Environment>>,
                    confver: Option<i64>,
                ) -> SchemeResult<LinkedHashMap<Rc<String>, Rc<RefCell<DynEnvironment>>>> 
{
    let mut env_list: LinkedHashMap<Rc<String>, Rc<RefCell<DynEnvironment>>> = LinkedHashMap::new();
    let mut verchecked = confver.is_none();

    for v in values.iter() 
    {
        let res = evaluate_value(v, env.clone())?;
        match res
        {
            Value::ResVerion(ver) =>
            {
                verchecked = true;

                if confver.is_some() == true && ver != confver.unwrap()
                {
                    scheme_runtime_error!("Init file version: '{}' is not matching the \
                                             internal config version: '{}'", ver, confver.unwrap());
                }
            },
            Value::ResInterpritator(argtitle, argdatatype) =>
            {
                if env_list.insert(argtitle.clone(), argdatatype).is_some() == true
                {
                    scheme_runtime_error!("Duplicate interpritator {}, near {}", 
                                            argtitle, v.extract_lexerinfo());
                }
            },
            _ => 
            {
                let li = match values[0]
                {
                    Value::List(_, ref li) => li.clone(),
                    _ => scheme_runtime_error!("Scheme should start from List, got {}, near: {}", 
                                                values[0], values[0].extract_lexerinfo()),
                };

                scheme_runtime_error!("Unexpected value {}, near {}", v, li);
            }
        } //match

    }

    if verchecked == false
    {
        scheme_runtime_error!("Version of the init file and config was not matched! Missing \
                                (version <number>)");
    }

    return Ok(env_list);
}

fn native_quoted(args: &[Value], _env: Rc<RefCell<Environment>>, li: &LexerInfo) -> SchemeResult<Value> 
{
    if args.len() < 1 
    {
        scheme_runtime_error!("native_quoted '() too many args todo near {}", li);
    }

    return Ok(args[0].clone());
}

fn f_version(args: &[Value], env: Rc<RefCell<Environment>>, li: &LexerInfo) -> SchemeResult<Value> 
{
    if args.len() != 1 
    {
        scheme_runtime_error!("Procedure (version) must supply  1 arg: <integer>, near: {}", li);
    }

    let mut reader: CommonReader = CommonReader::new(env.clone(), li, args, 0);

    let ver = reader.common_read_int()?;
    reader.eof()?;

    return Ok(Value::ResVerion(ver));
}

fn f_serializator(args: &[Value], env: Rc<RefCell<Environment>>, li: &LexerInfo) -> SchemeResult<Value> 
{
    if args.len() < 2 
    {
        scheme_runtime_error!("Procedure (serializator) must supply > 2 args: <argument_type | list>, near: {}", li);
    }

    let mut reader: CommonReader = CommonReader::new(env.clone(), li, args, 0);

    let title = Rc::new(reader.common_read_string()?);

    let mut rootproc: Option<Rc<SchemeProcedure>> = None;
    let mut values: LinkedHashMap<String, DynValue> = LinkedHashMap::new();
    let mut defbindings: LinkedHashMap<String, HashSet<String>> = LinkedHashMap::new();

    reader.evaluate_list(true, 0, |list, lenv, lli| 
        {
            let eval = evaluate_expression(list, lenv.clone(), lli)?;
            match eval
            {
                Value::RetDefine(def_name, def_data, def_bind) =>
                {
                    let val_name = def_name;
                    let val_dynval = def_data.into();
                    
                    if values.contains_key(&val_name) == true
                    {
                        scheme_runtime_error!("Duplicate (define \"{}\") near: {}",
                                                val_name, lli);
                    } 
                    else 
                    {
                        values.insert(val_name.clone(), val_dynval);

                        if let Some(bindings) = def_bind
                        {
                            if defbindings.contains_key(&val_name) == true
                            {
                                panic!("assertion: duplicate key: '{}' in defbindings", val_name);
                            }
                            else 
                            {
                                defbindings.insert(val_name, bindings);
                            }
                        }
                    }
                },
                Value::RetProcedure(proc_scheme) =>
                {
                    let p_name = proc_scheme.clone_title();

                    if values.contains_key(&p_name) == true
                    {
                        scheme_runtime_error!("Duplicate (procedure \"{}\", near: {}",
                                                p_name, lli);
                    } 
                    else 
                    {
                        values.insert(p_name, DynValue::Procedure(DynFunction::Dynamic(Rc::new(proc_scheme))));

                    }
                },
                Value::RetRootProcedure(root_proc_scheme) =>
                {
                    if rootproc.is_some() == true
                    {
                        scheme_runtime_error!("Duplicate (rootprocedure), near: {}",
                                                lli);
                    }
                    else
                    {
                        rootproc = Some(Rc::new(root_proc_scheme));
                    }
                },
                _ => 
                {
                    scheme_runtime_error!("Unexpected result returned while parsing (serializator \"{}\"), near: {}",
                                            title, lli);
                }
            }

            return Ok(());
        })?;

    reader.eof()?;

    if rootproc.is_none() == true
    {
        scheme_runtime_error!("(rootprocedure) was not defined for (serializator \"{}\"), near: {}",
                                title, li);
    }

    let dynenv = 
        DynEnvironment::new_root(
                title.clone(), 
                rootproc.take().unwrap(), 
                values, 
                defbindings,
            );

    return Ok(Value::ResInterpritator(title, dynenv));
}

fn f_rootprocedure(args: &[Value], env: Rc<RefCell<Environment>>, li: &LexerInfo) -> SchemeResult<Value> 
{
    let res = f_procedure(args, env, li)?;

    match res
    {
        Value::RetProcedure(p) => return Ok(Value::RetRootProcedure(p)),
        _ => scheme_runtime_error!("Expected (rootprocedure), got: {}, near: {}", 
                                    res, li),
    }
}

fn f_procedure(args: &[Value], env: Rc<RefCell<Environment>>, li: &LexerInfo) -> SchemeResult<Value> 
{
    if args.len() < 2 
    {
        scheme_runtime_error!("(procedure) must supply >= 3 args: <procedure_name> <serialization_name> <args(...) ...> \
                                near: {}", li);
    }

    let mut reader: CommonReader = CommonReader::new(env.clone(), li, args, 0);

    let func_name = reader.common_read_string()?;

    // check if flag optional is set
    let func_empty = match reader.may_read_symbol()?
    {
        Some(r) =>
        {
            if r.is_proc_empty() == true
            {
                true
            }
            else
            {
                scheme_runtime_error!("(procedure <title> <opt:symbol>) expected String, (or) then Symbol 'p/empty' \
                                        got: {}, near: {}", r, li);
            }
        },
        None => false
    };

    let mut temp_sch_proc = SchemeProcedure::new(func_name.clone(), func_empty);

    // a storage to find duplicates
    let mut serialz_dups: HashSet<String> = HashSet::new();

    // the field order check list
    let mut ord_check: Vec<usize> = Vec::new();

    reader.evaluate_list(true, 0, |list, lenv, lli| 
        {
            let eval = evaluate_expression(list, lenv.clone(), lli)?;

            match eval
            {
                Value::RetArg(title, datatype, subdatatype, serializ) =>
                {
                    //find duplicates of the name field
                    if serializ.is_anonymous() == false
                    {
                        if serialz_dups.insert(serializ.clone_field_name()) == false
                        {
                            scheme_runtime_error!("(arg ... \"{}\") has duplicated \
                                serialization field name: '{}', in procedure: '{}' \
                                near: {}", title, serializ.get_field_title(),
                                func_name, lli);
                        }
                    }
                    else
                    {
                        if serialz_dups.insert(title.clone()) == false
                        {
                            scheme_runtime_error!("(arg ... \"{}\") has anonymous \
                                serialization field name: 'anonymous', however is \
                                duplicated by the arg title: '{}', in procedure: '{}' \
                                near: {}", title, title, func_name, lli);
                        }
                    }

                    //find duplicates for order number
                    let ordnum = serializ.get_field_ord();
                    
                    find_dups_for_ord_num(ordnum, &mut ord_check)?;
                    
                    let pv: ProcVar = ProcVar::new(datatype, subdatatype, serializ);
                    if temp_sch_proc.insert_arg(title.clone(), pv) == false
                    {
                        scheme_runtime_error!("Duplicate title for argument: '{}', in \
                            procedure: {}, near: {}", title, func_name, lli);
                    }
                },
                Value::RetProc(titles, flag, serializ) =>
                {
                    if serializ.is_anonymous() == false
                    {
                        if serialz_dups.insert(serializ.clone_field_name()) == false
                        {
                            scheme_runtime_error!("(proc '({})) has duplicated \
                                serialization field name: '{}', in procedure: '{}' \
                                near: {}", convert_hashset::<String>(&titles, " "), 
                                serializ.get_field_title(),
                                func_name, lli);
                        }
                    }
                    else
                    {
                        if let Some(dp) = temp_sch_proc.proc_search_match(&titles)
                        {
                            scheme_runtime_error!("(proc '({})) has anonymous \
                                serialization field name: '{}', but has duplicated \
                                proc names in procedure: '{}' \
                                near: {}", convert_hashset::<String>(&titles, " "), 
                                serializ.get_field_title(),
                                func_name, lli);
                        }
                    }

                    //find duplicates for order number
                    let ordnum = serializ.get_field_ord();

                    find_dups_for_ord_num(ordnum, &mut ord_check)?;

                    let sp = SchemeProc::new(titles, flag, serializ);
                    if let Some(tt) = temp_sch_proc.insert_procs(sp)
                    {
                        scheme_runtime_error!("Duplicated (proc ...) near: {}",
                                                lli);
                    }
                },
                /*Value::ComputeRet(comp) =>
                {
                    if compute.is_some() == true
                    {
                        return Err(SchemeError::Duplicate(DuplicateErr::Compute(list.to_vec(), li.clone())));
                    }

                    compute = Some(comp);
                    //proc.set_compute(comp)?;
                },*/
                _ => scheme_runtime_error!("Evaluated unexpected value: {}, near: {}",
                                            eval,  lli)
            }

            return Ok(());
        })?;

    reader.eof()?;

    if temp_sch_proc.is_empty() == true && temp_sch_proc.is_set_empty() == false
    {
        scheme_runtime_error!("(procedure \"{}\") is empty! Consider using symbol: 'p/empty' \
                            after title, near: {}", func_name, li);
    }

    //search for the gaps in the field order number
    let mut i = 0;
    loop
    {
        if (i+1) >= ord_check.len()
        {
            break;
        }

        let dif = ord_check[i+1] - ord_check[i];

        if dif >= 2
        {
            scheme_runtime_error!("(procedure \"{}\") serialization numbering issue, \
                    order is broken: next: {}, prev: {}, near: {}", 
                    func_name, ord_check[i+1], ord_check[i], li);
        }
        else if dif <= 0
        {
            scheme_runtime_error!("(procedure \"{}\") serialization numbering issue, \
                    order is broken (negative): next: {}, prev: {}, near: {}", 
                    func_name, ord_check[i+1], ord_check[i], li);
        }

        i += 1;
    }
    
    return Ok(Value::RetProcedure(temp_sch_proc));
}

fn f_proc(args: &[Value], env: Rc<RefCell<Environment>>, li: &LexerInfo) -> SchemeResult<Value> 
{
    if args.len() < 2 
    {
        scheme_runtime_error!("(proc) must supply >= 2 args: <symbol> <string>, near: {}",
                                li);
    }

    let mut reader: CommonReader = CommonReader::new(env.clone(), li, args, 0);
    
    let titles = reader.common_read_hset_string_nodup()?;
    let mut opts: Option<SchemeArgFlags> = None;
    let mut serialize: Option<SerializeAs> = None;

    if reader.is_eof() == false
    {
        //try to read procs RetFieldAs
        reader.evaluate_list(true, 0, |list, lenv, lli| 
            {
                let eval = evaluate_expression(list, lenv.clone(), lli)?;
    
                match eval
                {
                    Value::RetFieldAs(sas) =>
                    {
                        // check for duplicates
                        if serialize.is_some() == true
                        {
                            scheme_runtime_error!("Duplicate (field/as), near: {}", lli);
                        }

                        //field can not be f/hashmap, f/vector, f/string, f/enum
                        sas.is_correct_for_proc(lli)?;

                        serialize = Some(sas);
                    },
                    Value::RetFlags(saf) =>
                    {
                        if opts.is_some() == true
                        {
                            scheme_runtime_error!("Duplicate (flags), near: {}", lli);
                        }

                        opts = Some(saf);
                    }
                    _ => scheme_runtime_error!("For (proc) evaluated unknown val: '{}' \
                            near: {}", eval, lli),
                }

                return Ok(());
            })?;    
    }

    if serialize.is_none() == true
    {
        scheme_runtime_error!("In (proc) missing serialization information (frield/as)\
                            near: {}", li);
    }

    if opts.is_none() == true
    {
        opts = Some(SchemeArgFlags::new());
    }

    return Ok(Value::RetProc(titles, opts.unwrap(), serialize.unwrap()));
}

fn f_flags(args: &[Value], 
            env: Rc<RefCell<Environment>>, 
            li: &LexerInfo) -> SchemeResult<Value> 
{
    if args.len() == 0 
    {
        scheme_runtime_error!("(flags) must supply at least 1 arg: <modificator_symbols>, \
                    near: {}", li);
    }

    let mut reader: CommonReader = CommonReader::new(env.clone(), li, args, 0);

    let mut opts: SchemeArgFlags = SchemeArgFlags::new();

    while reader.is_eof() == false
    {
        let opt = reader.common_read_symbol()?;

        match opt
        {
            Value::OptionalRet => opts.set_optional(),
            Value::RepeatRet => opts.set_repeat(),
            _ => scheme_runtime_error!("For (flags) evaluated unknown val: {}, near {}",
                            opt, reader.get_prev_lexerinfo()),
        }
    }

    return Ok(Value::RetFlags(opts));
}

fn f_arg(args: &[Value], 
        env: Rc<RefCell<Environment>>, 
        li: &LexerInfo) -> SchemeResult<Value> 
{
    if args.len() < 2 
    {
        scheme_runtime_error!("(arg) must supply >= 2 args: <symbol> <string>, \
                    near: {}", li);
    }

    let mut reader: CommonReader = CommonReader::new(env.clone(), li, args, 0);

    let datatype = reader.common_read_varenum()?;
    let title = reader.common_read_string()?;
    let mut serialize: Option<SerializeAs> = None;

    let subtypedata: ProcVarTypeOpts = match datatype
    {
        ProcVarType::Vector => 
        {
            let gentype = reader.common_read_varenum()?;

            match gentype
            {
                ProcVarType::Vector => 
                    scheme_runtime_error!("(arg ... \"{}\") declared as vector can not have nested \
                                            vector, near: {}", title, li),
                _ => {}
            }

            ProcVarTypeOpts::Vector(gentype)
        },
        _ => ProcVarTypeOpts::None
    };


    if reader.is_eof() == false
    {
        //try to read procs RetFieldAs
        reader.evaluate_list(true, 0, |list, lenv, lli| 
            {
                let eval = evaluate_expression(list, lenv.clone(), lli)?;
    
                match eval
                {
                    Value::RetFieldAs(sas) =>
                    {
                        if serialize.is_some() == true
                        {
                            scheme_runtime_error!("(arg ... \"{}\") Duplicate (field/as) \
                                                 near: {}", title, lli);
                        }

                        //field can not be f/hashmap, f/enum
                        if sas.can_use_for_arg() == false
                        {
                            scheme_runtime_error!("(arg ... \"{}\") Type {} can not be used \
                                                with (arg), \
                                                near: {}", title, sas, lli);
                        }

                        serialize = Some(sas);
                    },
                    _ => scheme_runtime_error!("(arg ... \"{}\") for (arg) evaluated unknown \
                                         value: {}, \
                                        near: {}", title, eval, lli),
                }

                return Ok(());
            })?;
    }

    reader.eof()?;

    if serialize.is_none() == true
    {
        scheme_runtime_error!("(arg ... \"{}\") is missing serialization information, near: {}",
                            title, li);
    }

    return Ok(Value::RetArg(title, datatype, subtypedata, serialize.unwrap()));
}

fn f_field_as(args: &[Value], 
                env: Rc<RefCell<Environment>>, 
                li: &LexerInfo) -> SchemeResult<Value> 
{
    if args.len() < 2 
    {
        scheme_runtime_error!("(field/as) must supply >= 2 args: <order> <datatype>, \
                            near: {}", li);
    }

    let mut reader: CommonReader = CommonReader::new(env.clone(), li, args, 0);

    let ord = reader.common_read_uint()? as usize;
    
    let field_name = reader.common_read_field_name()?;

    let dt = reader.common_read_dt()?;

    reader.eof()?;

    //let ft = FieldType::new(to_datatype, opt_key, opt_val);

    let sas = SerializeAs::new(ord, field_name, dt);

    return Ok(Value::RetFieldAs(sas));
}

#[inline]
fn f_dt_list(reader: &mut CommonReader) -> SchemeResult<Box<FieldVarArg>> 
{
    let list_datatype = reader.common_read_fieldenum()?;

    let ret = match list_datatype
    {
        FieldVarType::Struct => FieldVarArg::Struct,
        FieldVarType::Field =>
        {
            FieldVarArg::Field(reader.common_read_string()?)
        },
        FieldVarType::Enum =>
        {
            FieldVarArg::Enum(f_dt_enumas(reader)?)
        },
        FieldVarType::Uinteger |
        FieldVarType::Integer |
        FieldVarType::String |
        FieldVarType::Boolean =>
        {
            // for the arg, convert elements of list to
            FieldVarArg::PrimArg(list_datatype)
        },
        _ => scheme_runtime_error!("Unknown (dt) datatype: {}, near: {}", 
                                    list_datatype, reader.get_prev_lexerinfo()),
    };

    return Ok(Box::new(ret));
}

#[inline]
fn f_dt_enumas(reader: &mut CommonReader) -> SchemeResult<LinkedHashMap<String, String>> 
{
    let mut enum_as: LinkedHashMap<String, String> = LinkedHashMap::new();

    //read proc to serlz conversions 
    reader.evaluate_list(true, 0, |list, lenv, lli| 
        {
            let eval = evaluate_expression(list, lenv.clone(), lli)?;

            match eval
            {
                Value::RetEnumAs(procname, serlzname) =>
                {
                    enum_as.insert(procname, serlzname);
                }, 
                _ => scheme_runtime_error!("Expected (enum/as) but was evaluated unknwon value: {} \
                                            near: {}", eval, lli),
            }

            return Ok(());
        })?;

    // check if enum members are stated
    if enum_as.is_empty() == true
    {
        scheme_runtime_error!("the enum requires that with (enum/as) are set the possible enum members \
                                near: {}", reader.get_prev_lexerinfo());
    }
    
    return Ok(enum_as);
}

fn f_dt(args: &[Value], env: Rc<RefCell<Environment>>, li: &LexerInfo) -> SchemeResult<Value> 
{
    if args.len() < 1 
    {
        scheme_runtime_error!("(dt) must supply 1 arg: <datatype>, near: {}", li);
    }

    let mut reader: CommonReader = CommonReader::new(env.clone(), li, args, 0);

    // i.e (dt f/hashmap "overkey" "overval")
    let to_datatype = reader.common_read_fieldenum()?;

    let ret = match to_datatype
    {
        FieldVarType::HashMap =>
        {
            //read key name (the foreign field name)
            let key_foreignfield_name = reader.common_read_string()?;

            //read value
            //let value_foreigndata_eval = reader.common_read_expression()?;

            // parse the value type
            let value_foreigndata_type = reader.common_read_fieldenum()?;

            let fsubtp = match value_foreigndata_type
            {
                FieldVarType::Field => 
                {
                    FieldVarArg::Field(reader.common_read_string()?)
                },
                FieldVarType::Struct =>
                {
                    FieldVarArg::Struct
                },
                FieldVarType::Enum =>
                {
                    //read enum/as
                    FieldVarArg::Enum(f_dt_enumas(&mut reader)?)
                },
                FieldVarType::List =>
                {
                    //read subtype
                    FieldVarArg::List(f_dt_list(&mut reader)?)
                },
                _ => scheme_runtime_error!("For (dt) was evaluated unknown type: '{}' \
                                    near: {}", value_foreigndata_type, 
                                    reader.get_prev_lexerinfo()),
            };

            FieldVarArg::HashMap(key_foreignfield_name, Box::new(fsubtp))
        },
        FieldVarType::List =>
        {
            FieldVarArg::List(f_dt_list(&mut reader)?)
        },
        FieldVarType::Enum =>
        {
            let enumas = f_dt_enumas(&mut reader)?;

            FieldVarArg::Enum(enumas)
        },
        FieldVarType::Struct =>
        {
            FieldVarArg::Struct
        },
        FieldVarType::Field =>
        {
            let foregnfieldname = reader.common_read_string()?;

            FieldVarArg::Field(foregnfieldname)
        },
        _ => 
        {
            //f/string f/integer f/uinteger f/boolean - does not have any args
            FieldVarArg::PrimArg(to_datatype)
        }
    };

    reader.eof()?;

    return Ok(Value::RetDt(ret));
}

fn f_enum_as(args: &[Value], env: Rc<RefCell<Environment>>, li: &LexerInfo) -> SchemeResult<Value> 
{
    if args.len() < 2 
    {
        scheme_runtime_error!("(enum/as) must supply 2 args: <proc_name> <serialz_name>, near: {}", li);
    }

    let mut reader: CommonReader = CommonReader::new(env.clone(), li, args, 0);
    let procname = reader.common_read_string()?;
    let serlzname = reader.common_read_string()?;

    reader.eof()?;

    return Ok(Value::RetEnumAs(procname, serlzname));
}

fn f_define(args: &[Value], env: Rc<RefCell<Environment>>, li: &LexerInfo) -> SchemeResult<Value> 
{
    if args.len() < 2 
    {
        scheme_runtime_error!("(define) must supply 2 args: <define_alias> \
                <define_data> optional:<procedure bindings vector>, near: {}", li);
    }

    let mut reader: CommonReader = CommonReader::new(env.clone(), li, args, 0);

    let def_name = reader.common_read_string()?;
    let def_data = Box::new(reader.common_read_sib()?);
    let def_bind = if args.len() > 2
        {
            Some(reader.common_read_hset_string_nodup()?)
        }
        else
        {
            None
        };
    
    reader.eof()?;
    
    return Ok(Value::RetDefine(def_name, def_data, def_bind));
}

