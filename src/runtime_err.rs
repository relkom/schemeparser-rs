/* 
 * schemeparser - scheme config serialization configurator
 * Copyright (C) 2021 Aleksandr Morozov, RELKOM s.r.o
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

use std::fmt;
use std::io;
use std::num;

use std::collections::HashSet;
use std::path::PathBuf;

use crate::lexer::lexer::LexerInfo;


#[derive(Debug)]
pub enum LexerError
{
    /// File open error
    LexFileOpenErr(String, io::Error),

    /// Empty scheme
    LexEmpty(PathBuf),

    /// Unexpected EOF in text, line, col, depth
    LexUnexpEof(PathBuf, LexerInfo, u64),

    /// Unexpected close pattern at depth 0
    LexUnexpClosePattern(PathBuf, LexerInfo),

    LexMissing(PathBuf, &'static str, LexerInfo, u64),

    LexSignUnsign(PathBuf, LexerInfo),

    LexUnexpBoolean(PathBuf, char, LexerInfo),

    LexUnexpEolStr(PathBuf, String, LexerInfo),

    LexEndOfQuoteExp(PathBuf, String, LexerInfo),

    LexNotAnInteger(PathBuf, String, LexerInfo),

    LexDelimExp(PathBuf, char, LexerInfo),

    LexUnexpectedChar(String, char, LexerInfo),
}

impl fmt::Display for LexerError 
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result 
    {
        match *self 
        {
            Self::LexFileOpenErr(ref path, ref err) => write!(f, "Lexer: File: {}, {}", path, err),
            Self::LexEmpty(ref path) => write!(f, "Lexer: File: {}, the scheme is empty!", path.to_string_lossy()),
            Self::LexUnexpEof(ref path, ref li, ref depth) => 
                write!(f, "Lexer: File: {}, unexpected EOF at {} iteration depth: {}", path.to_string_lossy(), li, depth),
            Self::LexUnexpClosePattern(ref path, ref li) => 
                write!(f, "Lexer:  File: {}, unexpected close ')' pattern at {}", path.to_string_lossy(), li),
            Self::LexMissing(ref path, ref name, ref li, ref depth) => 
                write!(f, "Lexer: File: {}, missing {} value at {}, depth: {}", path.to_string_lossy(), name, li, depth),
            Self::LexSignUnsign(ref path, ref li) => 
                write!(f, "Lexer: File: {}, unsigned Integer can not have '-', either remove 'u|U' or '-' at {}", path.to_string_lossy(), li),
            Self::LexUnexpBoolean(ref path, ref c, ref li) => 
                write!(f, "Lexer:  File: {}, for boolean type is expected 't' or 'f', but found {} at {}", path.to_string_lossy(), c, li),
            Self::LexUnexpEolStr(ref path, ref st, ref li) => 
                write!(f, "Lexer:  File: {}, unexpected EOF while parsing string: '{}' at {}", path.to_string_lossy(), st, li),
            Self::LexEndOfQuoteExp(ref path, ref st, ref li) => 
                write!(f, "Lexer:  File: {}, expected end of quote but found EOF while parsing '{}' at {}", path.to_string_lossy(), st, li),
            Self::LexNotAnInteger(ref path, ref st, ref li) => 
                write!(f, "Lexer:  File: {}, failed parsing text to integer for '{}' at {}", path.to_string_lossy(), st, li),
            Self::LexDelimExp(ref path, ref c, ref li) => 
                write!(f, "Lexer:  File: {}, expected delimeter, found '{}' at {}", path.to_string_lossy(), c, li),
            Self::LexUnexpectedChar(ref path, ref ch, ref li) =>
                write!(f, "Lexer:  File: {}, expected char '{}' at {}", path, ch, li),
        }
    }
}

pub struct SchemeRuntimeError 
{
    message: String,
}

impl SchemeRuntimeError
{
    pub fn new(msg: String) -> Self
    {
        return SchemeRuntimeError{message: msg};
    }
}

impl fmt::Display for SchemeRuntimeError 
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result 
    {
        write!(f, "{}", self.message)
    }
}
impl fmt::Debug for SchemeRuntimeError 
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result 
    {
        write!(f, "{}", self.message)
    }
}



pub type SchemeResult<T> = Result<T, SchemeError>;

#[derive(Debug)]
pub enum SchemeError 
{
    Io(io::Error),
    LexerErr(LexerError),
    SerderJsonErr(serde_json::Error),
    SedreJson(serde_json::error::Error),
    Utf8Error(std::str::Utf8Error),
    //CidrErr(cidr::NetworkParseError),
    ParseIntErr(num::ParseIntError),

    SchmRuntimeErr(SchemeRuntimeError),
}

impl fmt::Display for SchemeError 
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result 
    {
        match *self 
        {
            SchemeError::Io(ref err) => err.fmt(f),
            SchemeError::LexerErr(ref err) => err.fmt(f),
            SchemeError::SerderJsonErr(ref err) => err.fmt(f),
            SchemeError::SedreJson(ref err) => err.fmt(f),
            SchemeError::Utf8Error(ref err) => err.fmt(f),
            //SchemeError::CidrErr(ref err) => err.fmt(f),
            SchemeError::ParseIntErr(ref err) => err.fmt(f),

            Self::SchmRuntimeErr(ref err) => err.fmt(f),
        }
    }
}

impl From<io::Error> for SchemeError 
{
    fn from(err: io::Error) -> SchemeError 
    {
        SchemeError::Io(err)
    }
}

impl From<LexerError> for SchemeError 
{
    fn from(err: LexerError) -> SchemeError 
    {
        SchemeError::LexerErr(err)
    }
}

impl From<serde_json::Error> for SchemeError 
{
    fn from(err: serde_json::Error) -> SchemeError 
    {
        SchemeError::SerderJsonErr(err)
    }
}

impl From<std::str::Utf8Error> for SchemeError 
{
    fn from(err: std::str::Utf8Error) -> SchemeError 
    {
        SchemeError::Utf8Error(err)
    }
}

/*impl From<cidr::NetworkParseError> for SchemeError 
{
    fn from(err: cidr::NetworkParseError) -> SchemeError 
    {
        SchemeError::CidrErr(err)
    }
}*/

impl From<num::ParseIntError> for SchemeError 
{
    fn from(err: num::ParseIntError) -> SchemeError 
    {
        SchemeError::ParseIntErr(err)
    }
}



impl From<SchemeRuntimeError> for SchemeError 
{
    fn from(err: SchemeRuntimeError) -> SchemeError 
    {
        SchemeError::SchmRuntimeErr(err)
    }
}

pub fn convert_list<T: fmt::Display>(extr: &Vec<T>) -> String
{
    let recs: Vec<String> = extr.iter().map(|v| format!("{}", v)).collect();

    return recs.join(" ");
}

pub fn convert_hashset<T: fmt::Display>(extr: &HashSet<T>, sep: &str) -> String
{
    let recs: Vec<String> = extr.iter().map(|v| format!("{}", v)).collect();

    return recs.join(sep);
}

#[macro_export]
macro_rules! scheme_runtime_error 
{
    ($($arg:tt)*) => (
        return std::result::Result::Err(SchemeError::SchmRuntimeErr(SchemeRuntimeError::new(format!($($arg)*))))
    )
}
