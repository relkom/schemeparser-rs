/* 
 * schemeparser - scheme config serialization configurator
 * Copyright (C) 2021 Aleksandr Morozov, RELKOM s.r.o
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


use schemeparser::runtime_err::SchemeResult;
use schemeparser::scheme::init::*;
use schemeparser::scheme::dynamic_scheme::{dyn_run}; 
use schemeparser::lexer::lexer::Lexer;
use linked_hash_map::LinkedHashMap;
use serde::{Serialize, Deserialize};

use std::time::Instant;

//use schemeparser::scheme::tests::test_serializer1::test_dyn_serializer4;

fn main() -> SchemeResult<()>
{    
   // test_dyn_serializer4();
       /* let start = Instant::now();
            let last = Lexer::from_file("/tmp/init.shm").unwrap(); //Lexer::from_str(t1).unwrap();
        let duration = start.elapsed();
        println!("Lexer: {:?}", duration);
    
        let start = Instant::now();
            let mut s = SchemeInit::new();
            let res = s.run(&last);
        let duration = start.elapsed();
        println!("Scheme pars: {:?}", duration);
    
        let start = Instant::now();
        let list = match res
        {
            Ok(r) => 
            {
                for (_k, v) in r.iter()
                {
                v.borrow().validate().unwrap();
    
                //dyn_print(&dynres);
                } 
    
                r
            },
            Err(e) =>
            {
                println!("err ({:?}): {}", e, e);
                panic!("e");
            }
        };
    
        let duration = start.elapsed();
        println!("Validation pars: {:?}", duration);
    */


        /*let req = "test".to_string();
        let pars = list.get(&req).unwrap();
    
        let test1ast = Lexer::from_str(v).unwrap();
    
        let start = Instant::now();
            let dynres = dyn_run(pars.clone(), &test1ast);
        let duration = start.elapsed();
        println!("dyn_run: {:?}", duration);

        assert_eq!(dynres.is_err(), true);

        //test error todo
        println!("error: {}", dynres.err().unwrap());
*/
        
    return Ok(());
}